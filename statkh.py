import sys
import math as m
import numpy as np

# Hyperexponential distribution
class	hyperex_gen:
	def __init__( self, name='hyperex' ):
		self.name =  name
		pass

	def freeze(self, p, beta ):
		self.p = p
		self.bata = beta
		pass	
	
# pdf:Probability Distribution Function
#	pdf(x) = sigma(i=0,n){p(i)* 1/beta(i)*exp(-1/beta(i)*x)}
	def pdf( self, x, p, beta ):

		if( p.size != beta.size ):
			raise Exception( self.name + ".pdf: size of propotion and beta must match" )

		if( type(x) == float ):
			pdf = 0.
			for i in xrange( p.size ):
				pdf += p[i]*1./beta[i]*m.exp( -1.0/beta[i] * x )
			return pdf

		elif( type(x) == np.ndarray ):
			pdf = np.empty([x.size])
			for k in xrange( x.size ):
				pdf[k] = 0.
				for i in xrange( p.size ):
					pdf[k] += p[i]*1./beta[i]*m.exp( -1.0/beta[i] * x[k] )
#				pdf[k] = m.fsum(  p*1./beta*m.exp( -1.0/beta * x[k] ))
			return pdf
		else:
			raise Exception( self.name + ".pdf: x data type must be float of numpy.ndarray")

# Cumulative Prob. Distribution Function
#	cdf(x) = sigma(i=0,n){p(i)* (1-exp(-1/beta(i)*x))}
	def cdf( self, x, p, beta ):

		if( p.size != beta.size ):
			raise Exception( self.name + ".cdf: size of propotion and beta must match" )

		if( type(x) == float ):
			cdf = self.cdf_1( x, p, beta )
			return cdf

		elif( type(x) == np.ndarray ):
			cdf = np.empty([x.size])
			for k in xrange( x.size ):
				cdf[k] = 0.
				for i in xrange( p.size ):
					cdf[k] += p[i]*(1 - m.exp( -1.0/beta[i] * x[k] ) )
			return cdf
		else:
			raise Exception( self.name + ".cdf: x data type must be float of numpy.ndarray")

# cdf for one float value
	def cdf_1( self, x, p, beta ):
		cdf = 0.
		for i in xrange( p.size ):
			cdf += p[i]*(1 - m.exp( -1.0/beta[i] * x ) )
		return cdf

# ppf: Inverse of cdf by binary search
	def ppf( self, cdf, p, beta ):

		if( p.size != beta.size ):
			raise Exception( self.name + ".cdf: size of propotion and beta must match" )

		if( type(cdf) == float ):
			x = self.ppf_1( cdf, p, beta )
			return x

		elif( type(cdf) == np.ndarray ):
			x = np.empty([cdf.size])
			for k in xrange( cdf.size ):
				x[k] = self.ppf_1( cdf[k], p, beta )
			return x
		else:
			raise Exception( self.name + ".ppf: cdf data type must be float of numpy.ndarray")

# ppf_1: ppf for one float value
	def	ppf_1( self, cdf, p, beta ):

		if( cdf < 0. or cdf >= 1.0 ):
			raise Exception( self.name + ".ppf_1: cdf must be ge 0 lt 1: %f"%cdf)

#	error threshold
		eps = 0.1e-6
#	maximum number of count for search
		countMax = 1000
		xmax = 0.
		for i in xrange( p.size ):
			xmax = max( xmax, -1.*beta[i] * m.log(1.-cdf) )
		cdfmax = self.cdf_1( xmax, p, beta )

		xmin = xmax
		for i in xrange( p.size ):
			xmin = min( xmin, -1.*beta[i] * m.log(1.-cdf) )
		cdfmin = self.cdf_1( xmin, p, beta )

		count = 0
		while( count < countMax ):
			x = (xmin + xmax ) /2.
			cdfx = self.cdf_1( x, p, beta ) 
			if( cdfx > cdf ):
				xmax = x
			else:
				xmin = x
			err = (cdfx - cdf )
#			print( "count %d, cdf = %f: x=%f, cdfx = %f err = %f"%(count, cdf, x, cdfx, err) )
			if( m.fabs(cdfx - cdf ) < eps ):
				return x

		raise Exception( self.name + ".ppf_1: could not find x for cdf %f"%cdf)

	def get_name( self ):
#		return self.name
		return 'hyperex'

hyperex = hyperex_gen( name = 'hyperex' )

if __name__ == "__main__":
	main()
