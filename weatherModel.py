import sys
#import math
from scipy.special import erf, erfinv
from scipy.stats import norm
from math import sqrt
import numpy as np
import matplotlib.pyplot as plt

PRG_NAME = "normbybnnnan"

#	normXfromCp gives x value that corresponds to cumulative prabability cp
#		cp: cumulative probability
def normXfromCp(cp):
	return norm.ppf( cp )
#	return sqrt(2.0)*erfinv(2.*cp - 1.)

#	normByBnnn gives mean and standarad deviagion of a normal distribution
#	that gives below normal and near normal probability
#		bnp: below normal probability .e.g. 0.25
#		nnp: near  normal probability .e.g. 0.45
#		anp: above normal prorability is not necessary. It is 1 - bnp - nnp
#
def normByBnnn( bnp, nnp ):
#	cumulative probability function value
#	cbnp: cumulative below normal  .e.g 0.25
#	cnnp: cumulative near  normal  .e.g 0.25 + 0.35
	cbnp = bnp
	cnnp = cbnp + nnp

	print( "cumulative porability threshold (cbnp, cnnp) = (%.5lf, %.5lf)"%(cbnp,cnnp))

#	usual cumulative below normal and near normal is 33.33% and 66.67%
	cbnp0 = 1./3.
	cnnp0 = 2./3.

#	bnx, nnx = x value corresponding to cumulative below normal and cumulative near normal
#	cumulative noraml dist(x) = PHI(x) = 1/2*(1+erf((x-myu)/(sigma*sqrt(2))))
#	myu = 0, sigma = 1.0
#	PHI(x) = 1/2*(1+erf(x/sqrt(2))) 
#	x corresponding to the borader or bn, nn, an, (bnx,nnx) = PHI_inv(bnp,nnp) 
#	PHI_inv(cp) = sqrt(2)*erf_inv(2*cp-1)

	bnx = normXfromCp( cbnp0 )
	nnx = normXfromCp( cnnp0 )
	print ( "x corresponding to equal range of bn,nn,an: (bnx,nnx) = (%.5f, %.5f)"%(bnx, nnx ))

#	sigma = (bnx-nnx)/sqrt(2.0)/(erfinv( 2.0*cbnp-1.)-erfinv(2.0*cnnp-1.))
#	myu   = bnx - sigma * sqrt(2.0) * erfinv( 2.0*cbnp-1.)
	sigma = (bnx-nnx)/( norm.ppf(cbnp)-norm.ppf(cnnp) )
	myu   = bnx - sigma * norm.ppf(cbnp)

	return myu, sigma

import argparse
def main():
	parser = argparse.ArgumentParser()
	parser.add_argument("bnp", type=float, help="below normal probability [0.,1.0]")
	parser.add_argument("nnp", type=float, help="near  normal probability [0.,1.0]")
	parser.add_argument("anp", type=float, help="above normal probability [0.,1.0]")
#	parser.add_argument("--graph", nargs=1, type=argparse.FileType('w'), help="--graph imageFileName")
	parser.add_argument("-show", action='store_true', help="-show is a switch to show graph on screen")

	args = parser.parse_args()

	bnp = args.bnp
	nnp = args.nnp
	anp = args.anp
#	file = args.graph
#	print("graph name " +  file[0].name )
	show = args.show

	print( "(bnp,nnp,anp) =(%.5f,%.5f,%.5f)"%(bnp,nnp,anp))
	if( bnp+nnp+anp != 1.0 ):
		print( PRG_NAME + ": bnp+nnp+anp must be 1.0" )
		print( "example: "+PRG_NAME + "0.25 0.35 0.40" )
		sys.exit()

	myu, sigma = normByBnnn( bnp, nnp )

	print( "{myu:%.5f, sigma:%.5f}"%(myu,sigma))

#	Testing
#	cbnp2, cnnp2 is the cumulative prob. for bn, nn
	bnx = normXfromCp( 1./3.0 )
	nnx = normXfromCp( 2./3.0 )
	cbnp2 = norm.cdf(bnx, loc=myu,scale=sigma)
	cnnp2 = norm.cdf(nnx, loc=myu,scale=sigma)

#	bnp2, nnp2, anp2 is the probability of bn, nn, an
	bnp2 = cbnp2
	nnp2 = cnnp2 - cbnp2
	anp2 = 1.0 - cnnp2
	print( "Test, These must be same as input: (%.5f,%.5f,%.5f)"%(bnp2, nnp2, anp2))

#	Plort Start
	x = np.linspace(-3,+3,1000)
	y0 = norm.pdf(x, loc=0, scale=1)
	y1 = norm.pdf(x, loc=myu, scale=sigma)
	plt.plot(x,y0,c='k')	# black line
	plt.plot(x,y1,c='r')	# red line
	plt.axvline(x=bnx,c='g', ls="dashed")	# green dasshed line
	plt.axvline(x=nnx,c='g', ls="dashed")	# green dasshed line
	print("saving figure as figpy.png")
	plt.savefig('figpy.png', bbox_inches='tight')	# save the graph as dist.png
	if show == True:
		plt.show()

	
if __name__ == "__main__":
	main()
