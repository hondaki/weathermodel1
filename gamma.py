from scipy.stats import gamma, norm, mstats
import matplotlib.pyplot as plt
import numpy as np
import weatherModel as wm
import scipy.stats as stats

#	convert gamma x to norm x that has same cdf value
def gm_x2nm_x( gm_x, alpha, beta ):
	gm_cdf = gamma.cdf( gm_x, alpha, scale = beta ) 
	nm_x = norm.ppf( gm_cdf )
	return	nm_x 

#	convert norm(myu,sigma) x to gamma x that has same cdf value
def nm_x2gm_x( nm_x, myu, sigma, alpha, beta ):
	nm_cdf = norm.cdf( nm_x, loc = myu, scale = sigma )
	gm_x = gamma.ppf( nm_cdf, alpha, scale = beta )
	return	gm_x 

#	cdf value by modified normal disribution(myu,sigma) 
def gm_cdf_modified( gm_x, alpha, beta, myu, sigma ):
	nm_x = gm_x2nm_x( gm_x, alpha, beta )
	nm_cdf = norm.cdf( nm_x, loc=myu, scale=sigma )
	return nm_cdf

#	give gamma pdf modified by myu and sigma
def gm_pdf_modified( gm_x, alpha, beta, myu, sigma ):
	nm_x = gm_x2nm_x( gm_x, alpha, beta )
	nm_pdf = norm.pdf( nm_x, loc=myu, scale=sigma )
	return nm_pdf

# generate gamma distribution moidifed by myu and sigma
#	if myu=0, sigma = 1, no modification
def gm_gen_variates_mod( alpha, beta, myu, sigma, size ):
	nm_variates = norm.rvs(loc=myu, scale=sigma, size = size )
	gm_variates = nm_x2gm_x( nm_variates, 0., 1., alpha, beta )
#	print( nm_variates)
#	print( gm_variates)
	return gm_variates


import argparse
def main():
	parser = argparse.ArgumentParser()

	parser.add_argument("alpha", type=float, help="alpha(shape) parameter")
	parser.add_argument("beta", type=float, help="beta(scale) parameter")
	parser.add_argument("bnp", type=float, help="below normal probability [0.,1.0]")
	parser.add_argument("nnp", type=float, help="near  normal probability [0.,1.0]")
	parser.add_argument("anp", type=float, help="above normal probability [0.,1.0]")
#	parser.add_argument("--graph", nargs=1, type=argparse.FileType('w'), help="--graph imageFileName")
	parser.add_argument("-show", action='store_true', help="-show is a switch to show graph on screen")

	args = parser.parse_args()

	alpha = args.alpha
	beta = args.beta
	bnp = args.bnp
	nnp = args.nnp
	anp = args.anp

#	cpf threshold to be adjusted
	cbnp = bnp
	cnnp = cbnp + nnp

#	file = args.graph
#	print("graph name " +  file[0].name )
	show = args.show

	cbnp0 = 1./3.
	cnnp0 = 2./3.
#	x:gamma that correspond to bn,nn threshold(1/3, 2/3)
	gm_bnx = normXfromCp( cbnp0, alpha, beta )
	gm_nnx = normXfromCp( cnnp0, alpha, beta )
	print ( "x corresponding to equal range of bn,nn,an: (gm_bnx,gm_nnx) = (%.5f, %.5f)"%(gm_bnx, gm_nnx ))

#	x:norm <- bn, nn, threhold ( of course -0.43073 and +0.43073 )
	nm_bnx = gm_x2nm_x(gm_bnx, alpha, beta )
	nm_nnx = gm_x2nm_x(gm_nnx, alpha, beta )
#	print ( "x corresponding to equal range of bn,nn,an: (nm_bnx,nm_nnx) = (%.5f, %.5f)"%(nm_bnx, nm_nnx ))

#	mean and sigma that gives cbnp, cnnp at bn, nn
	myu, sigma = wm.normByBnnn( bnp, nnp )
	print ( "myu and sigma : (myu,sigma) = (%.5f, %.5f)"%(myu, sigma ))

#	Testing
#	cbnp2, cnnp2 is the cumulative prob. for bn, nn
	gm_bnx = normXfromCp( 1./3.0, alpha, beta  )
	gm_nnx = normXfromCp( 2./3.0, alpha, beta  )
	cbnp2 = gm_cdf_modified( gm_bnx, alpha, beta, myu, sigma )
	cnnp2 = gm_cdf_modified( gm_nnx, alpha, beta, myu, sigma )

#	bnp2, nnp2, anp2 is the probability of bn, nn, an
	bnp2 = cbnp2
	nnp2 = cnnp2 - cbnp2
	anp2 = 1.0 - cnnp2
	print( "Test, These must be same as input: (%.5f,%.5f,%.5f)"%(bnp2, nnp2, anp2))


#	Generate distribution data
#	gm_x = np.linspace( 0.001, 5, 100 )
	gm_x = np.linspace(gamma.ppf(0.01, alpha, scale=beta), gamma.ppf(0.99, alpha, scale=beta), 100)
	gm_xmax = gamma.ppf(0.99,alpha,scale=beta)
	print( "gm_xmax (cdf %f) =%f"%(0.99,gm_xmax))

#	graph section
#	gamma pdf 
	gm_pdf = gamma.pdf(gm_x, alpha, scale = beta)

#	gamma cdf 
	gm_cdf = gamma.cdf(gm_x, alpha, scale = beta)

#	gamma x -> norm x
	nm_xgm = gm_x2nm_x(gm_x, alpha, beta)

#	norm x -> back to gamma x for testing
	gm_xgm2 = nm_x2gm_x(nm_xgm, 0., 1.0, alpha, beta)

#	norm cdf vs gamma x for testing
	nm_cdf_gm = norm.cdf(nm_xgm)

#	norm pdf vs gamma x
	dx = gm_x[1]-gm_x[0]
	nm_pdf_gm = np.gradient( nm_cdf_gm, dx )

#	gamma cdf modified via norm
	gm_cdf_mod = gm_cdf_modified(gm_x, alpha, beta, myu, sigma )

#	gamma pdf modified via norm
	dx = gm_x[1]-gm_x[0]
	gm_pdf_mod = np.gradient( gm_cdf_mod, dx )

	fig = plt.figure( "Gamma<->Normal Distribution BN:NN:AN=%.2f:%.2f:%.2f"%(bnp,nnp,anp), figsize=(8,10))
	fig.suptitle( "Gamma<->Normal Distribution  BN:NN:AN=%.2f:%.2f:%.2f"%(bnp,nnp,anp) )
	figNline = 3
	ax = fig.add_subplot(figNline,2,1)
	ax.plot( gm_x, gm_pdf, 'r', lw=1, label = "gamma pdf")

	ax = fig.add_subplot(figNline,2,2)
	ax.plot( gm_x, gm_cdf, 'r', lw=1, label = "gamma cdf")

	ax = fig.add_subplot(figNline,2,3)
	ax.plot( gm_x, nm_xgm, 'g', lw=1, label = "gamma x -> norm x")

	ax = fig.add_subplot(figNline,2,4)
	ax.plot( gm_x, gm_xgm2, 'g', lw=1, label = "gamma x -> norm x -> gamma x")

	ax = fig.add_subplot(figNline,2,2)
	ax.plot( gm_x, nm_cdf_gm, 'g', lw=1, label = "gamma x -> norm x -> norm cdf")

	ax = fig.add_subplot(figNline,2,1)
	ax.plot( gm_x, nm_pdf_gm, 'g', lw=1, label = "gamma x -> norm x -> norm pdf")

	ax = fig.add_subplot(figNline,2,2)
	ax.plot( gm_x, gm_cdf_mod, 'y', lw=1, label = "gamma x -> modified cdf")
	ax.axhline(y=cbnp,c='g', ls="dashed")	# green dasshed line
	ax.axhline(y=cnnp,c='g', ls="dashed")	# green dasshed line
	ax.axvline(x=gm_bnx,c='g', ls="dashed")	# green dasshed line
	ax.axvline(x=gm_nnx,c='g', ls="dashed")	# green dasshed line

	ax = fig.add_subplot(figNline,2,1)
	ax.plot( gm_x, gm_pdf_mod, 'y', lw=1, label = "gamma x -> modified pdf")

	legend = "norm(myu=%.2f,sigma=%.2f)"%(myu,sigma)
	ax = fig.add_subplot(figNline,2,6)
	ax.plot( nm_xgm, nm_cdf_gm, 'g', lw=1, label = "norm(myu=0,sigma=1)")
	ax.plot( nm_xgm, norm.cdf(nm_xgm, loc=myu, scale = sigma), 'y', lw=1, label = legend)
	ax.axhline(y=cbnp,c='g', ls="dashed")	# green dasshed line
	ax.axhline(y=cnnp,c='g', ls="dashed")	# green dasshed line
	ax.axvline(x=nm_bnx,c='g', ls="dashed")	# green dasshed line
	ax.axvline(x=nm_nnx,c='g', ls="dashed")	# green dasshed line

	ax = fig.add_subplot(figNline,2,5)
	ax.plot( nm_xgm, norm.pdf(nm_xgm, loc=0, scale = 1 ), 'g', lw=1, label = "norm(myu=0,sigma=1)")
	ax.plot( nm_xgm, norm.pdf(nm_xgm, loc=myu, scale = sigma), 'y', lw=1, label = legend )
	ax.axvline(x=nm_bnx,c='g', ls="dashed")	# green dasshed line
	ax.axvline(x=nm_nnx,c='g', ls="dashed")	# green dasshed line

	for i in xrange(figNline*2):
		ax = fig.add_subplot(figNline,2,i+1)
		plt.legend(loc='best', fontsize = 10, frameon=False)

#	plt.tight_layout()
	print("saving figure as gamma_dist.png")
	plt.savefig('gamma_dist.png', bbox_inches='tight')	# save the graph as gamma_dist.png


	nSample = 1000
	gm_variates_mod = gm_gen_variates_mod( alpha, beta, myu, sigma, nSample )
	gm_variates = gamma.rvs( alpha, scale = beta, size= nSample )
	xmax0=max(gm_variates)
	xmin0=min(gm_variates)
	xmax1=max( gm_variates_mod )
	xmin1=min( gm_variates_mod )

	xmax = max( xmax0, xmax1)
	ixmax = int(xmax+1)
	print( "max is (%f,%f) %f imax is %d"%(xmax0, xmax1, xmax,ixmax))

	mean, var, skew, kurt = gamma.stats(alpha, scale=beta, moments='mvsk')
	print( mean, var, skew, kurt )
	mean0 = np.mean( gm_variates )
	std0 = np.std( gm_variates )
	var0 = np.var( gm_variates )
	skew0 = stats.skew( gm_variates )
	kurt0 = stats.kurtosis( gm_variates )
	print( "original gamma: mean, std, skw, kur, min, max =%.1f %.1f %.1f %.1f %.1f %.1f"%(mean0, std0, skew0, kurt0, xmin0, xmax0 ))

	mean1 = np.mean( gm_variates_mod )
	std1 = np.std( gm_variates_mod )
	var1 = np.var( gm_variates_mod )
	skew1 = stats.skew( gm_variates_mod )
	kurt1 = stats.kurtosis( gm_variates_mod )
	print( "adjusted gamma: mean, std, skw, kur, min, max =%.1f %.1f %.1f %.1f %.1f %.1f"%(mean1, std1, skew1, kurt1, xmin1, xmax1 ))
 

	xaxmax = ixmax
	fighist = plt.figure("Histogram BN:NN:AN=%.2f:%.2f:%.2f"%(bnp,nnp,anp))
	fighist.suptitle( "Histogram BN:NN:AN=%.2f:%.2f:%.2f N=%d"%(bnp,nnp,anp,nSample) )

	ax = fighist.add_subplot( 2, 2, 1, title = 'count(original)')
	ax.hist( gm_variates, normed=False, histtype='stepfilled', alpha=0.2, bins=40, range=(0,ixmax))
	ax.set_xlim((0,ixmax))

	ax = fighist.add_subplot( 2, 2, 3, title = 'count(original) in bn,nn,an')
	ax.hist( gm_variates, normed=False, histtype='stepfilled', alpha=0.2, bins=(0,gm_bnx, gm_nnx,ixmax ), range=(0,ixmax))
	ax.set_xlim((0,ixmax))

	ax = fighist.add_subplot( 2, 2, 2, title = 'count(adjusted)')
	ax.hist( gm_variates_mod, normed=False, histtype='bar', alpha=0.2, bins=40, range=(0,ixmax))
	ax.set_xlim((0,ixmax))

	ax = fighist.add_subplot( 2, 2, 4, title = 'count(adjusted) in bn,nn,an')
	ax.hist( gm_variates_mod, normed=False, histtype='bar', alpha=0.2, bins=(0,gm_bnx, gm_nnx,ixmax ), range=(0,ixmax))
	ax.set_xlim((0,ixmax))
#	ax.set_title('count')



#	print( gm_variates_mod )
	nbn = sum( gm_variates < gm_bnx )
	nbnp = float(nbn) / gm_variates.size
	print ( "nbnp(x<%f)=%f (%d/%d)"%(gm_bnx, nbnp, nbn, gm_variates.size) )
	nnn = sum( gm_variates < gm_nnx ) - nbn
	nnnp = float(nnn) / gm_variates.size
	print ( "nnnp(%f<x<%f)=%f (%d/%d)"%(gm_bnx, gm_nnx, nnnp, nnn, gm_variates.size) )
	nan = sum( gm_variates >= gm_nnx )
	nanp = float(nan) / gm_variates.size
	print ( "nanp(%f<x)=%f (%d/%d)"%(gm_nnx, nanp, nan, gm_variates.size) )

	nbn = sum( gm_variates_mod < gm_bnx )
	nbnp = float(nbn) / gm_variates_mod.size
	print ( "nbnp(x<%f)=%f (%d/%d)"%(gm_bnx, nbnp, nbn, gm_variates_mod.size) )
	nnn = sum( gm_variates_mod < gm_nnx ) - nbn
	nnnp = float(nnn) / gm_variates_mod.size
	print ( "nnnp(%f<x<%f)=%f (%d/%d)"%(gm_bnx, gm_nnx, nnnp, nnn, gm_variates_mod.size) )
	nan = sum( gm_variates_mod >= gm_nnx )
	nanp = float(nan) / gm_variates_mod.size
	print ( "nanp(%f<x)=%f (%d/%d)"%(gm_nnx, nanp, nan, gm_variates_mod.size) )

	
	head0 = "Gamma (N, alpha, beta)=(%d,%.5e,%.5e)"%(nSample,alpha,beta)
	head1 = "Gamma (N, alpha, beta)=(%d,%.5e,%.5e), adjusted to (BN,NN,AN)=(%.3f,%.3f,%.3f)"%(nSample,alpha,beta,bnp,nnp,anp)
	print( "saving original gamma variates to gamma_original.csv")
	np.savetxt("gamma_original.csv", gm_variates, fmt="%.1f", delimiter=',', newline='\n', header=head0)
	print( "saving adjusted gamma variates to gamma_adjusted.csv")
	np.savetxt("gamma_adjusted.csv", gm_variates_mod, fmt="%.1f", delimiter=',', newline='\n', header=head1)
	print("saving figure as gamma_hist.png")
	plt.savefig('gamma_hist.png', bbox_inches='tight')	# save the graph as gamma_hist.png
#	plt.show()
	if show == True:
		plt.show()

def normXfromCp(cp, a, beta):
	return gamma.ppf( cp, a, scale = beta )

if __name__ == "__main__":
	main()
