import numpy as np
from scipy.stats import gamma, norm, mstats
from statkh import hyperex

def main():

	y = norm.pdf(1.0)
	print( y )

	p = np.array([0.3,0.7 ])
	beta = np.array([0.5, 1.2])
	hep_x = np.linspace( 0.001, 5, 1000 )

	hep_pdf = hyperex.pdf( hep_x, p, beta )
	hep_cdf = hyperex.cdf( hep_x, p, beta )
#	hp = hyperex()
#	hep_pdf = hp.pdf( 1.0, p, beta )
#	hep_pdf = hp.pdf( hep_x, p, beta )
	
	print( hep_x )
	print( hep_pdf )
	print( hep_cdf )
	print( hyperex.get_name() )

#	hep_pdf = hyperex.pdf( 5.56444444e-01, p, beta )
#	hep_cdf = hyperex.cdf( 5.56444444e-01, p, beta )
#	print( hep_pdf )
#	print( hep_cdf )

#	x = hyperex.ppf( hep_cdf, p, beta )
#	print (x)
#	x = hyperex.ppf( 1., p, beta )
#	print (x)
	x2 = hyperex.ppf( hep_cdf, p, beta )
	print (x2)
	err = x2-hep_x
	print (err)

main()
