#!/home/ec2-user/anaconda2/envs/pycrop35/bin/python
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
plt.ioff()

import matplotlib.patches as patches
import numpy as np
import datetime, time
import matplotlib.dates as mdates
from matplotlib.dates import MO, TU, WE, TH, FR, SA, SU


#import arrow
import decimal

#import pylab
import os, sys

#import cgi
#import cgitb; cgitb.enable()

#from matplotlib.backends.backend_agg import FigureCanvasAgg
#from django.http import HttpResponse

###########################################
# generate a multiple-boxplot image
#	categories:	list of categories to plot e.g. ['early planting', 'standard planting', 'late planting']
#	direction: 'h' for horizontal boxplot, 'v' for vertical boxplot(usual) default is 'h'
#	vtype: if data type = dateTime, then specify 'date', deafult is float
#	vmin, vmax: minimum and maximum plotted of axis, default is 0 and 1
#	size: None for auto adjust. Image size array in inch  e.g. (10,5), default is none
#	dpi: dpi used when saving as a file default is 100
class boxplot_viewgraph:
	def	__init__( self, categories, dataName=None, direction='h', vtype = 'float', vmin = 0., vmax = 1., size=None, dpi=100 ):
		self.direction = direction
		self.vmin = vmin
		self.vmax = vmax
		self.vtype = vtype
		self.categories=categories
		self.nBoxplot = len( categories )
		self.dataName = dataName
		if( size is None ):
			if( direction == 'h' ):
				self.fig = plt.figure(figsize=(10,1*len(categories)))
			else:
				self.fig = plt.figure(figsize=(1*(len(categories)+2),5))
		else:
			self.fig = plt.figure(figsize=size)
		self.dpi = dpi
		self.ax = self.fig.add_subplot(111)

		self.whisker_len = 0.1
		self.box_width=0.2

		if( self.vtype == 'date' ):
#			print('setting locator and formatter' )
			self.majorLoc = mdates.MonthLocator()
			self.majorFmt = mdates.DateFormatter('%-m/%d')
			self.minorLoc = mdates.WeekdayLocator(byweekday=(MO))
#			print('vmin,vmax=', self.vmin, self.vmax )

###########################################
#	add a boxplot
# 	category: one of the categories used when boxplot_viewgraph was generated. If does not match added to the categories
#	x: data, if vtype is 'date', then should be datetime datatyupe
#	facecolor: color to fillup the box, default is beige
#	show_outlier: if True, calculate outliers less than Q1-1.5IRQ and Q3+1.5IRQ, default is False
#	low_outlier_color, high_outlier_color: The colors of outlier markers, default is maroon

	def add_box( self, category, xraw, facecolor='beige', show_outlier=False, low_outlier_color='maroon', high_outlier_color='maroon' ):
		try:
			idx = self.categories.index(category)
		except:
			self.nBoxplot = self.nBoxplot + 1
			self.categories.append(category)
			idx = self.nBoxplot-1

#		self.draw_box( idx, x, facecolor, show_outlier, low_outlier_color, high_outlier_color )

#	internal function called by self::add_box
#	def draw_box( self, idx, xraw, facecolor, show_outlier, low_outlier_color, high_outlier_color ):

#		q0 = np.percentile(x,0)
#		q1 = np.percentile(x,25)
#		q2 = np.percentile(x,50)
#		q3 = np.percentile(x,75)
#		q4 = np.percentile(x,100)

#		copy data for sorting
		x = list(xraw)
		nd = len(x)
		x.sort()
#		http://www.itl.nist.gov/div898/handbook/prc/section1/prc16.htm
#		need to strictly follow above altorithm, if date object, then cannot calculate the middle day
		q0 = x[  0]
		q1 = x[ int(nd/4.)  ]
		q2 = x[ int(nd/2)  ]
		q3 = x[ int(nd/4.*3.)]
		q4 = x[ int(nd-1) ]

#		print(q0,q1,q2,q3,q4)

		whisker_low  = q0
		whisker_high = q4
		iqr = q3-q1
		if( self.vtype == 'date' ):
#			print( 'iqr.total_seconds()', iqr.total_seconds()/(3600.*24.) )
#			q1s = time.mktime(q1.timetuple())
			epoch = datetime.date(1970, 1, 1)
			q1s = (q1-epoch).total_seconds()/(3600.*24.)
#			print( epoch )
#			print('q1s=',q1s)
#			print( time.gmtime(0))
			box_len = iqr.total_seconds()/(3600.*24.)
		else:
			q1s = q1
			box_len = iqr

		outlier_low=[]
		outlier_high=[]
		if( show_outlier is True ):
			if( self.vtype != 'date' ):
				for xi in x:
					if( xi < q2-iqr*1.5):
						outlier_low.append(xi)
					if( xi >= q2-iqr*1.5):
						whisker_low = xi
						break
				for xi in reversed(x):
					if( xi > q3+iqr*1.5):
						outlier_high.append(xi)
					if( xi <= q3+iqr*1.5):
						whisker_high = xi
						break
#				print( whisker_high, whisker_low)
			else:
				iqr15_seconds = iqr.total_seconds()*1.5
				low_thresh = q1 + datetime.timedelta( seconds = -iqr15_seconds )
				high_thresh = q3 + datetime.timedelta( seconds = iqr15_seconds )
#				print( iqr, iqr15_seconds, low_thresh )
				for xi in x:
					if( xi < low_thresh):
						outlier_low.append(xi)
					if( xi >= low_thresh):
						whisker_low = xi
						break
				for xi in reversed(x):
					if( xi > high_thresh):
						outlier_high.append(xi)
					if( xi <= high_thresh):
						whisker_high = xi
						break

#				print('whisker =', whisker_low, whisker_high )
#				print('outlier_low =', outlier_low )
#				print('outlier_high =', outlier_high )
					
				pass

		if( self.direction == 'h' ):
			ycenter = idx
#			color : http://matthiaseisen.com/pp/patterns/p0203/, http://matplotlib.org/users/colors.html
			self.ax.plot( (whisker_low,  whisker_low ), (ycenter-self.whisker_len/2,ycenter+self.whisker_len/2), color='k', zorder=0 )
			self.ax.plot( (whisker_high, whisker_high), (ycenter-self.whisker_len/2,ycenter+self.whisker_len/2), color='k', zorder=0 )
			self.ax.plot( (whisker_low,  q1), (ycenter,ycenter), color='k', zorder=0 )
			self.ax.plot( (q3,  whisker_high), (ycenter,ycenter), color='k', zorder=0 )
#			patches.Rectangle does not accept datetime.date object as a coordinate on linux... it works on mac though...
#			self.ax.add_patch( patches.Rectangle( (q1,ycenter-self.box_width/2.), box_len , self.box_width, facecolor=facecolor, edgecolor="black" ) )
			y1 = ycenter-self.box_width/2.
			y2 = ycenter+self.box_width/2.
			self.ax.plot( (q1,q1,q3,q3,q1), (y1, y2, y2, y1, y1), color='k', zorder=0 )
			self.ax.plot( (q2,  q2 ), (ycenter-self.box_width/2,ycenter+self.box_width/2), color='k', zorder=10 )
			for outlier in outlier_low:
				self.ax.plot( [outlier], [ycenter], 'o', color=low_outlier_color )
			for outlier in outlier_high:
				self.ax.plot( [outlier], [ycenter], 'o', color=high_outlier_color )
		else:
			ycenter = idx
			self.ax.plot( (ycenter-self.whisker_len/2,ycenter+self.whisker_len/2), (whisker_low,  whisker_low ), color='k', zorder=0 )
			self.ax.plot( (ycenter-self.whisker_len/2,ycenter+self.whisker_len/2), (whisker_high, whisker_high), color='k', zorder=0 )
			self.ax.plot( (ycenter,ycenter), (whisker_low,  q1), color='k', zorder=0 )
			self.ax.plot( (ycenter,ycenter), (q3,  whisker_high), color='k', zorder=0 )
#			self.ax.add_patch( patches.Rectangle( (ycenter-self.box_width/2., q1), self.box_width, box_len, facecolor=facecolor, edgecolor="black" ) )
			y1 = ycenter-self.box_width/2.
			y2 = ycenter+self.box_width/2.
			self.ax.plot( (y1, y2, y2, y1, y1), (q1,q1,q3,q3,q1), color='k', zorder=0 )
			self.ax.plot( (ycenter-self.box_width/2,ycenter+self.box_width/2), (q2, q2 ), color='k', zorder=10 )
#			print( 'outlier_low =', outlier_low)
#			print( 'outlier_high =', outlier_high)
			for outlier in outlier_low:
				self.ax.plot( [ycenter], [outlier], 'o', color=low_outlier_color )
			for outlier in outlier_high:
				self.ax.plot( [ycenter], [outlier], 'o', color=high_outlier_color )

		qtiles=[q0,q1,q2,q3,q4]
		whisker=[whisker_low,whisker_high]
		return  nd, qtiles, whisker, outlier_low, outlier_high
		pass

###########################################
#	obtain image
#	show: If True then show on screen
#	fName: if not None, image is saved as a file
#	data:  if True, this function returs binary data block
#
	def draw(self, show=True, fName=None ):
		if( self.direction == 'h' ):
#			print('direction=', self.direction )
			self.ax.xaxis.set_major_locator(self.majorLoc)
			self.ax.xaxis.set_major_formatter(self.majorFmt)
			self.ax.xaxis.set_minor_locator(self.minorLoc)
			self.ax.set_xlim( self.vmin, self.vmax )
			self.ax.set_ylim( -0.5, self.nBoxplot-0.5 )
			if( self.vtype == 'date' ):
#				print('vtype', 'date' )
#				self.fig.autofmt_xdate()
#				https://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.set_yticks.html
                            pass
			self.ax.set_yticks([i for i in range(self.nBoxplot)],False)
			self.ax.set_yticklabels( self.categories )
			if( self.dataName is not None ):
				self.ax.set_xlabel( self.dataName )
			self.ax.xaxis.grid()
		else:
			self.ax.set_ylim( self.vmin, self.vmax )
			self.ax.set_xlim( -0.5, self.nBoxplot-0.5 )
			self.ax.set_xticks([i for i in range(self.nBoxplot)],False)
			self.ax.set_xticklabels( self.categories )
			if( self.dataName is not None ):
				self.ax.set_ylabel( self.dataName )
			self.ax.yaxis.grid()

		self.fig.tight_layout()
		if( show is True ):
			plt.show()
		if( fName is not None ):
			if( fName is sys.stdout ):
				canvas = FigureCanvasAgg(self.fig)
				response = HttpResponse( content_type='image/png')
				canvas.print_png(response)
				response.write(sys.stdout)
#				print( response )

#				tmpfile = '/tmp/tempfile.png'
#				pylab.savefig( tmpfile, format='png' )
#				import shutil
#				fin = open( tmpfile, 'rb' )
#				contents = fin.read()
#				print('Content-Type: image/png\n\n')
#				print('Content-Length: %d\r\n\r\n'%len(contents))
##				https://stackoverflow.com/questions/21689365/python-3-typeerror-must-be-str-not-bytes-with-sys-stdout-write
##				sys.stdout.write(contents)
#				sys.stdout.buffer.write(contents)
##				shutil.copyfileobj(open( tmpfile,'rb'), sys.stdout)
#				os.remove( tmpfile )
			else:
				self.fig.savefig( fName, dpi=self.dpi )
	


def	json_dumps( js ):

	JSONEncoder_olddefault = json.JSONEncoder.default
	def JSONEncoder_newdefault(self, o):
#		if isinstance(o, UUID): return str(o)
		if isinstance(o, datetime.datetime): return str(o)
		if isinstance(o, datetime.date): return o.strftime('%Y-%m-%d')
		if isinstance(o, time.struct_time): return datetime.fromtimestamp(time.mktime(o))
#		if isinstance(o, decimal.Decimal): return str(o)
		if isinstance(o, decimal.Decimal): return float(str(o))
		return JSONEncoder_olddefault(self, o)

	json.JSONEncoder.default = JSONEncoder_newdefault

	return json.dumps(js)

def parse_arg( args ):


	err = 0
	response={}
	response['erros'] = []
	try:
		dataName = args['dataName']
	except:
		dataName = None
	response['dataName']=dataName

	try:
		direction = args['direction']
	except:
		direction = None
	response['direction']=direction

	try:
		categories = args['categories']
	except:
		err = err+1
		response['erros'].append('categories is missing')
	response['categories']=categories
	try:
		vtype = args['vtype']
	except:
		vtype = 'float'
	response['vtype']= vtype

	try:
		vmin = args['vmin']
	except:
		vmin = None

	if( vmin is not None ):
		if( vtype == 'date' ):
			try:
				dt = datetime.datetime.strptime(vmin,'%Y-%m-%d')
				vmin = datetime.date(dt.year, dt.month, dt.day )
#				response['vmin']= vmin.strftime('%Y-%m-%d')
				response['vmin']= vmin
			except:
				err = err+1
				response['erros'].append('vmin parse error')
		else:
			vmin = float(vmin)
			response['vmin']= vmin

	try:
		vmax = args['vmax']
	except:
		vmax = None

	if( vmax is not None ):
		if( vtype == 'date' ):
			try:
				dt = datetime.datetime.strptime(vmax,'%Y-%m-%d')
				vmax = datetime.date(dt.year, dt.month, dt.day )
#				response['vmax']= vmax.strftime('%Y-%m-%d')
				response['vmax']= vmax
			except:
				err = err+1
				response['erros'].append('vmax parse error')
		else:
			vmax = float(vmax)
			response['vmax']= vmax
	try:
		size = args['size']
	except:
		size = None
	response['size']= size

	try:
		dpi = int(args['dpi'])
	except:
		dpi = None
	response['dpi']= dpi

	dataset=[]
	for series in args['dataset']:
		data=[]
		for x in series['data']:
			if(vtype =='date'):
				dt = datetime.datetime.strptime(x,'%Y-%m-%d')
				data.append( datetime.date(dt.year, dt.month, dt.day))
			else:
				data.append( float(x) )
		dataset.append({'category':series['category'],'data':data})
#		print( series['category'] , 'ndata = ', len( data ))
#	print( dataset )
			

#	print( json_dumps(response) )

	return response, dataset
		

import sys, json
def cgi(fName):

	data=sys.stdin.read()
	args=json.loads(data)
	r, dataset = parse_arg(args)

	bplot = boxplot_viewgraph(  r['categories'], dataName=r['dataName'], direction=r['direction'], vtype=r['vtype'], vmin = r['vmin'], vmax = r['vmax'], size=None, dpi=r['dpi'] )

	bdata={'result':[], 'dataName':r['dataName']}
	for series in dataset:
		nd, qtiles, whisker, out_low, out_high = bplot.add_box( series['category'], series['data'],  show_outlier=True )
		bdata1={}
		bdata1['category']=series['category']
		bdata1['quartiles'] = qtiles
		bdata1['fences'] = whisker
		bdata1['outliers_low']=out_low
		bdata1['outliers_high']=out_high
		bdata1['n']=nd
		bdata['result'].append( bdata1 )
#	print( json_dumps(bdata) )
	bplot.draw( show=False, fName = fName )
	return bdata


def main1( fName ):

# Calnedar day box plot example
		
	dateMin = datetime.date(2017, 4,1)
	dateMax = datetime.date(2017,11,1)

#	sample date data
	deltaDates = np.random.normal(0., 4., 200)
	dates1=[]
	for deltaDate in deltaDates:
		dates1.append(datetime.date(2017,6,23) + datetime.timedelta( days = deltaDate ))
	dates1.append(datetime.date(2017,6,1))
	
	deltaDates = np.random.normal(0., 6., 100)
	dates2=[]
	for deltaDate in deltaDates:
		dates2.append(datetime.date(2017,6,30) + datetime.timedelta( days = deltaDate ))
	
	deltaDates = np.random.normal(0., 6., 100)
	dates3=[]
	for deltaDate in deltaDates:
		dates3.append(datetime.date(2017,8,30) + datetime.timedelta( days = deltaDate ))
	
# 	generate box plot viewgraph for t'%Y-%m-%d'wo categories

	bplot = boxplot_viewgraph(  ['5/1', '4/24'], dataName='flowering and maturity date', direction='h', vtype='date', vmin = dateMin, vmax = dateMax, size=None, dpi=None )
	#bplot = boxplot_viewgraph(  ['5/1', '4/24'], direction='v', vtype='date', vmin = dateMin, vmax = dateMax, size=(10,10), dpi=100 )
	#bplot = boxplot_viewgraph(  ['5/1', '4/24'], direction='v', vtype='date', vmin = dateMin, vmax = dateMax, size=None, dpi=100 )
	#bplot.set_size( 10, 1*4, dpi=100 )
	bplot.add_box( '5/1', dates2,  show_outlier=True )
	bplot.add_box( '4/24', dates1, show_outlier=True )
	bplot.add_box( '4/24', dates3, show_outlier=True )
#	bplot.draw( show=False, fName = 'boxplot-h.png' )
	bplot.draw( show=False, fName = fName )
	
def main2():
	# Usual numerial data
	data1 = np.random.normal(5000,300,100)
	data2 = np.random.normal(5500,180,100)
	data3 = np.random.normal(5200,150,100)
	bplot = boxplot_viewgraph(  ['4/24', '5/1', '5/8'], dataName='yield [kg/ha]', direction='v', vmin = 3000, vmax = 6000, size=None, dpi=100 )
	bplot.add_box( '4/24', data1, show_outlier = True, low_outlier_color='r', high_outlier_color='green' )
	bplot.add_box( '5/1', data2, show_outlier = True, low_outlier_color='red', high_outlier_color='yellow' )
	bplot.add_box( '5/8', data3, facecolor='blue', show_outlier = True, low_outlier_color='violet', high_outlier_color='black' )
	bplot.draw( show=False, fName = 'boxplot-v.png' )


if __name__ == "__main__":
	os.environ[ 'HOME'] = '/tmp/'

#	print ("Content-type:text/html\r\n\r\n")
#	print('Content-Type:application/json\n\n')
	
#	print ("Content-type:text/html\r\n\r\n")
#	outFile = sys.stdout
#	outFile = 'tmp.png'
#	if( outFile is sys.stdout ):
#		print('idential')
#	else:
#		print('different')
#	main1( outFile )

#	main1('/var/www/html/boxplot/boxplot.png')

	bdata = cgi('/var/www/html/boxplot/boxplot.png')

	url = 'http://api.listenfield.com/boxplot/'
	bdata['boxplot_link']= url + 'boxplot.png' 

	print('Content-Type:application/json\n\n')
	print( json_dumps(bdata) )
	
#	print( "<img href="+url + '/boxplot/boxplot.png'+">")
