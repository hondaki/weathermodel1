# generate vertical boxplot
echo 'generating vertical box plot'
curl -s -X POST 'http://api.listenfield.com/cgi-bin/boxplot_cgi.py' -H "Content-Type: application/json" -d @boxplot-v.json

# generate horizongal boxplot and show the result
echo 'generating horizontal box plot and format by jq'
curl -s -X POST 'http://api.listenfield.com/cgi-bin/boxplot_cgi.py' -H "Content-Type: application/json" -d @boxplot-h.json | jq '[.]'

# generate horizontal boxplot and download , jq -r option is to remove the double quotations
echo 'generating horizontal boxplot, extract image link, then download, rename'
LINK=`curl -s -X POST 'http://api.listenfield.com/cgi-bin/boxplot_cgi.py' -H "Content-Type: application/json" -d @boxplot-h.json | jq -r '.boxplot_link'`
echo 'boxplot_link is ' $LINK

FNAME=`basename $LINK`
echo 'filenmae is ' $FNAME
rm -f $FNAME

echo 'getting the image by wget'
wget $LINK
mv $FNAME boxplot-h.png


