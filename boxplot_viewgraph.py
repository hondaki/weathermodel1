import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import datetime, time
import matplotlib.dates as mdates
from matplotlib.dates import MO, TU, WE, TH, FR, SA, SU

###########################################
# generate a multiple-boxplot image
#	categories:	list of categories to plot e.g. ['early planting', 'standard planting', 'late planting']
#	direction: 'h' for horizontal boxplot, 'v' for vertical boxplot(usual) default is 'h'
#	vtype: if data type = dateTime, then specify 'date', deafult is float
#	vmin, vmax: minimum and maximum plotted of axis, default is 0 and 1
#	size: None for auto adjust. Image size array in inch  e.g. (10,5), default is none
#	dpi: dpi used when saving as a file default is 100
class boxplot_viewgraph:
	def	__init__( self, categories, dataName=None, direction='h', vtype = 'float', vmin = 0., vmax = 1., size=None, dpi=100 ):
		self.direction = direction
		self.vmin = vmin
		self.vmax = vmax
		self.vtype = vtype
		self.categories=categories
		self.nBoxplot = len( categories )
		self.dataName = dataName
		if( size is None ):
			if( direction == 'h' ):
				self.fig = plt.figure(figsize=(10,1*len(categories)))
			else:
				self.fig = plt.figure(figsize=(1*(len(categories)+2),5))
		else:
			self.fig = plt.figure(figsize=size)
		self.dpi = dpi
		self.ax = self.fig.add_subplot(111)

		self.whisker_len = 0.1
		self.box_width=0.2

		if( self.vtype == 'date' ):
#			print('setting locator and formatter' )
			self.majorLoc = mdates.MonthLocator()
			self.majorFmt = mdates.DateFormatter('%-m/%d')
			self.minorLoc = mdates.WeekdayLocator(byweekday=(MO))
#			print('vmin,vmax=', self.vmin, self.vmax )

###########################################
#	add a boxplot
# 	category: one of the categories used when boxplot_viewgraph was generated. If does not match added to the categories
#	x: data, if vtype is 'date', then should be datetime datatyupe
#	facecolor: color to fillup the box, default is beige
#	show_outlier: if True, calculate outliers less than Q1-1.5IRQ and Q3+1.5IRQ, default is False
#	low_outlier_color, high_outlier_color: The colors of outlier markers, default is maroon

	def add_box( self, category, x, facecolor='beige', show_outlier=False, low_outlier_color='maroon', high_outlier_color='maroon' ):
		try:
			idx = self.categories.index(category)
		except:
			self.nBoxplot = self.nBoxplot + 1
			self.categories.append(category)
			idx = self.nBoxplot-1

		self.draw_box( idx, x, facecolor, show_outlier, low_outlier_color, high_outlier_color )

#	internal function called by self::add_box
	def draw_box( self, idx, xraw, facecolor, show_outlier, low_outlier_color, high_outlier_color ):

#		q0 = np.percentile(x,0)
#		q1 = np.percentile(x,25)
#		q2 = np.percentile(x,50)
#		q3 = np.percentile(x,75)
#		q4 = np.percentile(x,100)

#		copy data for sorting
		x = list(xraw)
		nd = len(x)
		x.sort()
		q0 = x[  0]
		q1 = x[ int(nd/4)  ]
		q2 = x[ int(nd/2)  ]
		q3 = x[ int(nd/4*3)]
		q4 = x[ int(nd-1) ]

#		print(q0,q1,q2,q3,q4)

		whisker_low  = q0
		whisker_high = q4
		iqr = q3-q1
		if( self.vtype == 'date' ):
#			print( 'iqr.total_seconds()', iqr.total_seconds()/(3600.*24.) )
			q1s = time.mktime(q1.timetuple())
#			print('q1s=',q1s)
			box_len = iqr.total_seconds()/(3600.*24.)
		else:
			q1s = q1
			box_len = iqr

		outlier_low=[]
		outlier_high=[]
		if( show_outlier is True ):
			if( self.vtype != 'date' ):
				for xi in x:
					if( xi < q2-iqr*1.5):
						outlier_low.append(xi)
					if( xi >= q2-iqr*1.5):
						whisker_low = xi
						break
				for xi in reversed(x):
					if( xi > q3+iqr*1.5):
						outlier_high.append(xi)
					if( xi <= q3+iqr*1.5):
						whisker_high = xi
						break
				print( whisker_high, whisker_low)
			else:
				iqr15_seconds = iqr.total_seconds()*1.5
				low_thresh = q1 + datetime.timedelta( seconds = -iqr15_seconds )
				high_thresh = q3 + datetime.timedelta( seconds = iqr15_seconds )
				print( iqr, iqr15_seconds, low_thresh )
				for xi in x:
					if( xi < low_thresh):
						outlier_low.append(xi)
					if( xi >= low_thresh):
						whisker_low = xi
						break
				for xi in reversed(x):
					if( xi > high_thresh):
						outlier_high.append(xi)
					if( xi <= high_thresh):
						whisker_high = xi
						break

				print('whisker =', whisker_low, whisker_high )
				print('outlier_low =', outlier_low )
				print('outlier_high =', outlier_high )
					
				pass

		if( self.direction == 'h' ):
			ycenter = idx
#			color : http://matthiaseisen.com/pp/patterns/p0203/, http://matplotlib.org/users/colors.html
			self.ax.plot( (whisker_low,  whisker_low ), (ycenter-self.whisker_len/2,ycenter+self.whisker_len/2), color='k', zorder=0 )
			self.ax.plot( (whisker_high, whisker_high), (ycenter-self.whisker_len/2,ycenter+self.whisker_len/2), color='k', zorder=0 )
			self.ax.plot( (whisker_low,  q1), (ycenter,ycenter), color='k', zorder=0 )
			self.ax.plot( (q3,  whisker_high), (ycenter,ycenter), color='k', zorder=0 )
			self.ax.add_patch( patches.Rectangle( (q1,ycenter-self.box_width/2.), box_len , self.box_width, facecolor=facecolor, edgecolor="black" ) )
			self.ax.plot( (q2,  q2 ), (ycenter-self.box_width/2,ycenter+self.box_width/2), color='k', zorder=10 )
			for outlier in outlier_low:
				self.ax.plot( [outlier], [ycenter], 'o', color=low_outlier_color )
			for outlier in outlier_high:
				self.ax.plot( [outlier], [ycenter], 'o', color=high_outlier_color )
		else:
			ycenter = idx
			self.ax.plot( (ycenter-self.whisker_len/2,ycenter+self.whisker_len/2), (whisker_low,  whisker_low ), color='k', zorder=0 )
			self.ax.plot( (ycenter-self.whisker_len/2,ycenter+self.whisker_len/2), (whisker_high, whisker_high), color='k', zorder=0 )
			self.ax.plot( (ycenter,ycenter), (whisker_low,  q1), color='k', zorder=0 )
			self.ax.plot( (ycenter,ycenter), (q3,  whisker_high), color='k', zorder=0 )
			self.ax.add_patch( patches.Rectangle( (ycenter-self.box_width/2., q1), self.box_width, box_len, facecolor=facecolor, edgecolor="black" ) )
			self.ax.plot( (ycenter-self.box_width/2,ycenter+self.box_width/2), (q2, q2 ), color='k', zorder=10 )
			print( 'outlier_low =', outlier_low)
			print( 'outlier_high =', outlier_high)
			for outlier in outlier_low:
				self.ax.plot( [ycenter], [outlier], 'o', color=low_outlier_color )
			for outlier in outlier_high:
				self.ax.plot( [ycenter], [outlier], 'o', color=high_outlier_color )

		pass

###########################################
#	obtain image
#	show: If True then show on screen
#	fName: if not None, image is saved as a file
#	data:  if True, this function returs binary data block
#
	def draw(self, show=True, fName=None ):
		if( self.direction == 'h' ):
#			print('direction=', self.direction )
			self.ax.xaxis.set_major_locator(self.majorLoc)
			self.ax.xaxis.set_major_formatter(self.majorFmt)
			self.ax.xaxis.set_minor_locator(self.minorLoc)
			self.ax.set_xlim( self.vmin, self.vmax )
			self.ax.set_ylim( -0.5, self.nBoxplot-0.5 )
			if( self.vtype == 'date' ):
#				print('vtype', 'date' )
#				self.fig.autofmt_xdate()

#				https://matplotlib.org/api/_as_gen/matplotlib.axes.Axes.set_yticks.html
				pass
			self.ax.set_yticks([i for i in range(self.nBoxplot)],False)
			self.ax.set_yticklabels( self.categories )
			if( self.dataName is not None ):
				self.ax.set_xlabel( self.dataName )
			self.ax.xaxis.grid()
		else:
			self.ax.set_ylim( self.vmin, self.vmax )
			self.ax.set_xlim( -0.5, self.nBoxplot-0.5 )
			self.ax.set_xticks([i for i in range(self.nBoxplot)],False)
			self.ax.set_xticklabels( self.categories )
			if( self.dataName is not None ):
				self.ax.set_ylabel( self.dataName )
			self.ax.yaxis.grid()

		self.fig.tight_layout()
		if( show is True ):
			plt.show()
		if( fName is not None ):
			self.fig.savefig( fName, dpi=self.dpi )
	

def main():

# Calnedar day box plot example
		
	dateMin = datetime.date(2017, 4,1)
	dateMax = datetime.date(2017,11,1)

#	sample date data
	deltaDates = np.random.normal(0., 4., 200)
	dates1=[]
	for deltaDate in deltaDates:
		dates1.append(datetime.date(2017,6,23) + datetime.timedelta( days = deltaDate ))
	dates1.append(datetime.date(2017,6,1))
	
	deltaDates = np.random.normal(0., 6., 100)
	dates2=[]
	for deltaDate in deltaDates:
		dates2.append(datetime.date(2017,6,30) + datetime.timedelta( days = deltaDate ))
	
	deltaDates = np.random.normal(0., 6., 100)
	dates3=[]
	for deltaDate in deltaDates:
		dates3.append(datetime.date(2017,8,30) + datetime.timedelta( days = deltaDate ))
	
# 	generate box plot viewgraph for two categories

	bplot = boxplot_viewgraph(  ['5/1', '4/24'], dataName='flowering and maturity date', direction='h', vtype='date', vmin = dateMin, vmax = dateMax, size=None, dpi=100 )
	#bplot = boxplot_viewgraph(  ['5/1', '4/24'], direction='v', vtype='date', vmin = dateMin, vmax = dateMax, size=(10,10), dpi=100 )
	#bplot = boxplot_viewgraph(  ['5/1', '4/24'], direction='v', vtype='date', vmin = dateMin, vmax = dateMax, size=None, dpi=100 )
	#bplot.set_size( 10, 1*4, dpi=100 )
	bplot.add_box( '5/1', dates2,  show_outlier=True )
	bplot.add_box( '4/24', dates1, show_outlier=True )
	bplot.add_box( '4/24', dates3, show_outlier=True )
	bplot.draw( show=True, fName = 'boxplot-h.png' )
	
	# Usual numerial data
	data1 = np.random.normal(5000,300,100)
	data2 = np.random.normal(5500,180,100)
	data3 = np.random.normal(5200,150,100)
	bplot = boxplot_viewgraph(  ['4/24', '5/1', '5/8'], dataName='yield [kg/ha]', direction='v', vmin = 3000, vmax = 6000, size=None, dpi=100 )
	bplot.add_box( '4/24', data1, show_outlier = True, low_outlier_color='r', high_outlier_color='green' )
	bplot.add_box( '5/1', data2, show_outlier = True, low_outlier_color='red', high_outlier_color='yellow' )
	bplot.add_box( '5/8', data3, facecolor='blue', show_outlier = True, low_outlier_color='violet', high_outlier_color='black' )
	bplot.draw( show=True, fName = 'boxplot-v.png' )


if __name__ == "__main__":
	main()
