#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

if (length(args)!=3) {
	stop(" specify bn, nn and an .n ", call.=FALSE)
}

bnp = as.numeric(args[1])
nnp = as.numeric(args[2])
anp = as.numeric(args[3])

if( bnp+nnp+anp != 1.0 ){
	stop("bn+nn+an must be 1.0 .n", call.=FALSE)
}

xmin=-3
xmax=+3
mean1 = 0
stdev1=1.0

#	cumulative probability function value
#	cbnp: cumulative below normal  .e.g 0.25
#	cnnp: cumulative near  normal  .e.g 0.25 + 0.35

	cbnp = bnp
	cnnp = cbnp + nnp
	annp = cnnp + anp

print( sprintf("cbnp, cnnp, annp = %f %f %f", cbnp, cnnp, anp ))

#	usual cumulative below normal and near normal is 33.33% and 66.67%
	cbnp0 = 1./3.
	cnnp0 = 2./3.

#	bnx, nnx = x value corresponding to cumulative below normal and cumulative near normal
#	cumulative noraml dist(x) = PHI(x) = 1/2*(1+erf((x-myu)/(sigma*sqrt(2))))
#	myu = 0, sigma = 1.0
#	PHI(x) = 1/2*(1+erf(x/sqrt(2))) 
#	x corresponding to the borader or bn, nn, an, (bnx,nnx) = PHI_inv(bnp,nnp) 
#	PHI_inv(cp) = sqrt(2)*erf_inv(2*cp-1)

	bnx = qnorm( cbnp0 )
	nnx = qnorm( cnnp0 )
	print( sprintf("x for bn, bn+nn = %f %f", bnx, nnx ))

#	sigma = (bnx-nnx)/sqrt(2.0)/(erfinv( 2.0*cbnp-1.)-erfinv(2.0*cnnp-1.))
#	myu   = bnx - sqrt(2.0) * sigma * erfinv( 2.0*cbnp-1.)
	sigma = (bnx-nnx)/( qnorm( cbnp )-qnorm( cnnp))
	myu   = bnx - sigma * qnorm(cbnp)

	print( sprintf("mean = %f sigam = %f", myu, sigma ))

	cbnp2 = pnorm(bnx, mean=myu,sd=sigma)
	cnnp2 = pnorm(nnx, mean=myu,sd=sigma)

#	bnp2, nnp2, anp2 is the probability of bn, nn, an
	bnp2 = cbnp2
	nnp2 = cnnp2 - cbnp2
	anp2 = 1.0 - cnnp2
	print( sprintf( "Test, These must be same as input: (%.5f,%.5f,%.5f)", bnp2, nnp2, anp2))

x <- seq(from=xmin, to=xmax, length.out=100)
dist1 <- dnorm(x, mean=mean1, sd=stdev1 )
dist2 <- dnorm(x, mean=myu,   sd=sigma  )
ymax = max( dist1, dist2 )
print( sprintf("ymax = %f", ymax ) )

#pdf(file="fig.pdf")
png(file="figr.png")
print("generating fig.png")
#plot(x, dnorm(x), type='l')
plot(x, dist1, type='l',ann=FALSE, ylim=c(0,ymax))
lines(x, dist2, type='l')
title(main="This is a normal dist graph", xlab="X", ylab="probability distribution function")
grid()
abline( v= bnx, lty="dashed" )
abline( v= nnx, lty="dashed" )
#abline( v= -2, lty="l")
dev.off()
