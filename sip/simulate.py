import json
import http.client
import datetime, time
import sys
import urllib.request
import copy

def	json_dumps( js ):
	JSONEncoder_olddefault = json.JSONEncoder.default
	def JSONEncoder_newdefault(self, o):
#		if isinstance(o, UUID): return str(o)
		if isinstance(o, datetime.datetime): return str(o)
		if isinstance(o, datetime.date): return o.strftime('%Y-%m-%d')
		if isinstance(o, time.struct_time): return datetime.fromtimestamp(time.mktime(o))
#		if isinstance(o, decimal.Decimal): return str(o)
		if isinstance(o, decimal.Decimal): return float(str(o))
		return JSONEncoder_olddefault(self, o)

	json.JSONEncoder.default = JSONEncoder_newdefault

	return json.dumps(js)


def wgen( lat=35.706179, lon = 140.482362, from_date="2017-01-01", to_date = "2017-12-31", scenario_num=100 ):

	conn = http.client.HTTPConnection("dev.listenfield.com")
	
	payload_json = {
		"wth_src"		: "naro1km",
		"wgen_model"	: "pdisagws",
		"scenario_num"	: "100",
		"latitude"		: "35.706179",
		"longitude"		: "140.482362",
		"from_date"		: "2017-01-01",
		"to_date"		: "2017-12-31",
		"bn_nn_an"		: "33:34:33"
	}

#	dt = datetime.date(2017,5,3)
#	dtstr=dt.strftime('%Y-%m-%d')
#	payload_json['from_date']=dtstr

	payload_json['latitude']=lat
	payload_json['longitude']=lon
	payload_json['from_date']=from_date
	payload_json['to_date']=to_date
	payload_json['scenario_num']=scenario_num
	
	
	payload  = json_dumps(payload_json)
	print( payload )
	
	headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "1cef0e9c-e5ac-a840-e7d2-f05c95bd2a38"
    }

	conn.request("POST", "/weather/generator/v1.1/scenarios", payload, headers)
	
	res = conn.getresponse()
	data = res.read()
	data_str = (data.decode("utf-8"))
	print(data_str)
	
	scenario_json = json.loads(data.decode("utf-8"))
	scenario_link = scenario_json['result']['downloadLink']
	scenario_id = scenario_json['ID']

	return scenario_id, scenario_link, scenario_json
	


def simriw(wth_scenario, pdate):
	conn = http.client.HTTPConnection("dev.listenfield.com")


	payload_json = {
		'model': 'simriw',
		'crop_ident_ICASA': 'RIC',
		'cultivar_name': 'Koshihikari',
		'transplant_date': '2017-05-26',
		'field_latitude': 35.706179,
		'field_longitude': 140.482362,
		'weather_file': 'http://dev.listenfield.com/weather/static/ex/20171119-055606-zQmCww.zip',
		'wait': True,
	}

	headers = {
	    'content-type': "application/json",
	    'cache-control': "no-cache",
	    'postman-token': "c10b5c00-ce7c-0566-842e-85417082cb31"
	    }

	payload_json['transplant_date'] = pdate.strftime('%Y-%m-%d')
	payload_json['weather_file'] = wth_scenario

	payload = json_dumps( payload_json )
#	print( payload )

#	return

	conn.request("POST", "/cropsim/v1.1/simulations", payload, headers)

	res = conn.getresponse()
	data = res.read()
	data_str = (data.decode("utf-8"))
	print(data_str)

	simresult_json = json.loads(data.decode("utf-8"))
	simresult_id = simresult_json['id']

	return simresult_id, simresult_json

'''
{
  "status": "Crop simulation succeed.",
  "id": "2017-11-19T07:28:18.810Z6bcc80fc8a6da2c4",
  "_self": "http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T07:28:18.810Z6bcc80fc8a6da2c4",
  "model": "simriw",
  "planting_date": null,
  "transplant_date": "2017-05-01",
  "crop_ident_ICASA": "RIC",
  "cultivar_name": "koshihikari",
  "weather_file": "http://dev.listenfield.com/weather/static/ex/20171119-065629-DGGFoB.zip",
  "raw_output": "http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T07:28:18.810Z6bcc80fc8a6da2c4/results/raw",
  "csv_output": "http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T07:28:18.810Z6bcc80fc8a6da2c4/results/csv",
  "json_output": "http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T07:28:18.810Z6bcc80fc8a6da2c4/results/json"
}
'''

#	scenario_json = json.loads(data.decode("utf-8"))
#	scenario_link = scenario_json['result']['downloadLink']
#	scenario_id = scenario_json['ID']

#	return scenario_id, scenario_link, scenario_json


def main():

#	scenario generated already
	scenario_num = 100
	wth_scenario_id = '0171119-065629-DGGFoB'
	wth_scenario = 'http://dev.listenfield.com/weather/static/ex/20171119-065629-DGGFoB.zip'

#	scenario_num = 200
#	wth_scenario_id = '20171119-135803-cjVdKv'
#	wth_scenario = 'http://dev.listenfield.com/weather/static/ex/20171119-135803-cjVdKv.zip'

#	new creation
#	scenario_num = 200
	wth_scenario_id, wth_scenario, wth_scenario_json =  wgen( lat=35.7062, lon = 140.4824, from_date="2017-01-01", to_date = "2017-12-31", scenario_num=10 )

	print( 'weather scenario id:', wth_scenario_id, wth_scenario )

#	return

	pdates=[]
	pdate0 = datetime.date(2017,5,8)
	pdates.append( pdate0 + datetime.timedelta(days = -7*2) )
	pdates.append( pdate0 + datetime.timedelta(days = -7*1) )
	pdates.append( pdate0                              )
	pdates.append( pdate0 + datetime.timedelta(days =  7*1) )
	pdates.append( pdate0 + datetime.timedelta(days =  7*2) )
	
	print( pdates )

	if 1:
		sim_ids =[]
		sim_results=[]
		for pdate in pdates:
			print('calling simriw', wth_scenario, pdate )
			sim_id_wk, sim_result_wk = simriw( wth_scenario, pdate )
			sim_ids.append( sim_id_wk )
			sim_results.append( sim_result_wk )

		print('simulation done' )

		idx = 0
		for pdate in pdates:
			print(sim_ids[idx], sim_results[idx]['status'] ) 
			idx = idx+1
	else:

# [datetime.date(2017, 4, 17), datetime.date(2017, 4, 24), datetime.date(2017, 5, 1), datetime.date(2017, 5, 8), datetime.date(2017, 5, 15)]

		sim_results=[
{"status":"Crop simulation succeed.","id":"2017-11-19T14:53:16.543Z011eb08580b1e6ab","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:16.543Z011eb08580b1e6ab","model":"simriw","planting_date":None,"transplant_date":"2017-04-24","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:16.543Z011eb08580b1e6ab/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:16.543Z011eb08580b1e6ab/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:16.543Z011eb08580b1e6ab/results/json"},
{"status":"Crop simulation succeed.","id":"2017-11-19T14:53:31.353Z4efd4c153b0e8e1d","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:31.353Z4efd4c153b0e8e1d","model":"simriw","planting_date":None,"transplant_date":"2017-05-01","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:31.353Z4efd4c153b0e8e1d/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:31.353Z4efd4c153b0e8e1d/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:31.353Z4efd4c153b0e8e1d/results/json"},
{"status":"Crop simulation succeed.","id":"2017-11-19T14:53:47.536Z4f8a631303d62982","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:47.536Z4f8a631303d62982","model":"simriw","planting_date":None,"transplant_date":"2017-05-08","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:47.536Z4f8a631303d62982/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:47.536Z4f8a631303d62982/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:47.536Z4f8a631303d62982/results/json"},
{"status":"Crop simulation succeed.","id":"2017-11-19T14:54:02.478Zeac51b05d60f94b2","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:02.478Zeac51b05d60f94b2","model":"simriw","planting_date":None,"transplant_date":"2017-05-15","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:02.478Zeac51b05d60f94b2/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:02.478Zeac51b05d60f94b2/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:02.478Zeac51b05d60f94b2/results/json"},
{"status":"Crop simulation succeed.","id":"2017-11-19T14:54:18.436Z3b08fbad9be300fe","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:18.436Z3b08fbad9be300fe","model":"simriw","planting_date":None,"transplant_date":"2017-05-22","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:18.436Z3b08fbad9be300fe/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:18.436Z3b08fbad9be300fe/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:18.436Z3b08fbad9be300fe/results/json"},
]

#		sim_results=[
#{"status":"Crop simulation succeed.","id":"2017-11-19T14:11:14.080Z5038bd746e7d7630","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:14.080Z5038bd746e7d7630","model":"simriw","planting_date":None,"transplant_date":"2017-04-22","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:14.080Z5038bd746e7d7630/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:14.080Z5038bd746e7d7630/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:14.080Z5038bd746e7d7630/results/json"},
#{"status":"Crop simulation succeed.","id":"2017-11-19T14:11:29.421Zf3028323c909a6a1","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:29.421Zf3028323c909a6a1","model":"simriw","planting_date":None,"transplant_date":"2017-04-29","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:29.421Zf3028323c909a6a1/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:29.421Zf3028323c909a6a1/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:29.421Zf3028323c909a6a1/results/json"},
#{"status":"Crop simulation succeed.","id":"2017-11-19T14:11:44.405Z6ff61758852acba0","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:44.405Z6ff61758852acba0","model":"simriw","planting_date":None,"transplant_date":"2017-05-06","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:44.405Z6ff61758852acba0/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:44.405Z6ff61758852acba0/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:44.405Z6ff61758852acba0/results/json"},
#{"status":"Crop simulation succeed.","id":"2017-11-19T14:12:00.696Z4651cac30abac6ba","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:00.696Z4651cac30abac6ba","model":"simriw","planting_date":None,"transplant_date":"2017-05-13","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:00.696Z4651cac30abac6ba/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:00.696Z4651cac30abac6ba/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:00.696Z4651cac30abac6ba/results/json"},
#{"status":"Crop simulation succeed.","id":"2017-11-19T14:12:17.936Z0bce0aaff71167d6","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:17.936Z0bce0aaff71167d6","model":"simriw","planting_date":None,"transplant_date":"2017-05-20","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:17.936Z0bce0aaff71167d6/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:17.936Z0bce0aaff71167d6/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:17.936Z0bce0aaff71167d6/results/json"},
#		]

	boxplot_template = {
			"dataName":"flowering date",
			"direction":"h",
			"vtype":"date",
			"vmin":"2017-04-01",
			"vmax":"2017-11-01",
			"size":[30,4],
			"dpi":200,
			"categories":[],
			"dataset":[]
	}
	series_template= {
			"category":None, "data":[]
	}

	categories = []
	dataset=[]
	boxplot_flowering = copy.deepcopy(boxplot_template)
	boxplot_flowering['dataName'] = 'Flowering Date'
	boxplot_flowering['vtype'] = 'date'
	boxplot_flowering['direction'] = 'h'

	boxplot_maturity  = copy.deepcopy(boxplot_template)
	boxplot_maturity ['dataName'] = 'Maturity Date'
	boxplot_maturity['vtype'] = 'date'
	boxplot_maturity['direction'] = 'h'

	boxplot_yield     = copy.deepcopy(boxplot_template)
	boxplot_yield    ['dataName'] = 'Yield [kg/ha]'
	boxplot_yield['vtype'] = 'float'
	boxplot_yield['direction'] = 'v'
	boxplot_yield['size'] = [20,10] 

	idx=0
	yldmin = sys.float_info.max
	yldmax = sys.float_info.min

	flwmin = None
	flwmax = None
	matmin = None
	matmax = None
	for sim_result in sim_results:
		pdate = datetime.datetime.strptime(sim_result['transplant_date'],'%Y-%m-%d' ).date()
		series_set_url = sim_result['json_output']
#		print( pdate, series_set_url )
		series = jsonread( series_set_url )
#		print( series )

		pdate_str = pdate.strftime('%-m/%d')
		flowering_dates = copy.deepcopy(series_template)
		flowering_dates['category'] = pdate_str
		boxplot_flowering['categories'].append( pdate_str )

		maturity_dates  = copy.deepcopy(series_template)
		maturity_dates['category'] = pdate_str
		boxplot_maturity['categories'].append( pdate_str )

		yields          = copy.deepcopy(series_template)
		yields        ['category'] = pdate_str
		boxplot_yield['categories'].append( pdate_str )

		idxx = 0
#		print(len(series))
		nd = len(series)
		for line in series:
#			print( idxx, line )
			if( idxx == 0 ):
				idxx = idxx+1
				continue
			idxx = idxx+1
			flowering_dates['data'].append(datetime.datetime.strptime(line[1],'%Y-%m-%d' ).date() )
			maturity_dates['data'].append( datetime.datetime.strptime(line[2],'%Y-%m-%d' ).date() )
			yields['data'].append(                                    line[3]                     )
			yld = float(line[3])
			if( yld < yldmin ):
				yldmin = yld
			if( yld > yldmax ):
				yldmax = yld
#			print( yld, yldmax )
		if( nd < scenario_num ):
#			n2add = int((scenario_num - nd)/2.25)
			n2add = int((scenario_num - nd))
			for _ in range(n2add):
				yields['data'].append( 0. )
			yldmin = 0.

		boxplot_flowering['dataset'].append( copy.deepcopy(flowering_dates))
		boxplot_maturity ['dataset'].append( copy.deepcopy(maturity_dates))
		boxplot_yield    ['dataset'].append( copy.deepcopy(yields))
		boxplot_flowering['vmin'] = datetime.date(2017,7,1)
		boxplot_flowering['vmax'] = datetime.date(2017,10,1)
		boxplot_maturity['vmin'] = datetime.date(2017,8,1)
		boxplot_maturity['vmax'] = datetime.date(2017,11,1)


	boxplot_flowering['categories'].reverse()
	boxplot_maturity['categories'].reverse()
#	nSimulation = len(sim_results)
#	for i in range(int(nSimulation/2)):
#		tmp = boxplot_flowering['dataset'][i]
#		boxplot_flowering['dataset'] = boxplot_flowering['dataset'][nSimulation-i-1]
#		boxplot_flowering['dataset'][nSimulation-i-1] = tmp

	yldmin = int(yldmin/1000)*1000.
	yldmax = (int(yldmax/1000)+1)*1000.
	boxplot_yield['vmin'] = yldmin
	boxplot_yield['vmax'] = yldmax

#	boxplot_flowering_payload = json_dumps(boxplot_flowering)
#	boxplot_maturity_payload  = json_dumps(boxplot_maturity)
#	boxplot_yield_payload     = json_dumps(boxplot_yield)

	genBoxplot( boxplot_flowering, 'boxplot_flowering.png' )
	genBoxplot( boxplot_maturity , 'boxplot_flowering.png' )
	genBoxplot( boxplot_yield    , 'boxplot_flowering.png' )

#	print(boxplot_flowering)
#	print(boxplot_maturity)
#	print(boxplot_yield)

#	print(boxplot_flowering_payload, 'boxplot_flowering.png')
#	print(boxplot_maturity_payload, 'boxplot_maturity.png')
#	print(boxplot_yield_payload, 'boxplot_yield.png')

#		print( sim_result )
#		print(data['transplant_date'] )
#		print( series )


def jsonread( url ):
	try:
		res = urllib.request.urlopen( url )
		data = json.loads( res.read().decode('utf-8') )
	except urllib.error.HTTPError as e:
		print ('HTTPError', e)
		sys.exit()
	except json.JSONDecodeError as e:
		print ('JSONDecodeError', e)
		sys.exit()

	return data


def genBoxplot(command, baseName ):

	conn = http.client.HTTPConnection("api.listenfield.com:80")
	
#	dt = datetime.date(2017,5,3)
#	dtstr=dt.strftime('%Y-%m-%d')
#	payload_json['from_date']=dtstr
	
	payload  = json_dumps(command)
	print( payload )
	
	headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    }

	conn.request("POST", "/cgi-bin/boxplot_cgi.py", payload, headers)
	
	res = conn.getresponse()
	data = res.read()
	data_str = (data.decode("utf-8"))
	print(data_str)
	
	bbox_json = json.loads(data.decode("utf-8"))
#	print( bbox_json )
	print( bbox_json['boxplot_link'] )

	pass

if __name__ == "__main__":
	main()

'''
simulation done
2017-11-19T07:48:09.296Z010211cf93c1be40 Crop simulation succeed.
2017-11-19T07:48:23.073Z1741f0f5bc9a11cd Crop simulation succeed.
2017-11-19T07:48:37.182Z5552c3d556c311ea Crop simulation succeed.
2017-11-19T07:48:52.776Z59972e424fdc35ae Crop simulation succeed.
2017-11-19T07:49:08.471Z3d21b79a66222920 Crop simulation succeed.
'''
