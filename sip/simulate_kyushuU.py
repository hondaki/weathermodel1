import datetime
import sys

# if result returns json, then these could be keys
#key_antehsis='adate'
#key_maturity='mdate'
#key_yield_act_brown_m15='AYBRON'
#key_yield_act_rough_m15='AYPADY'

# import agroapi SDK
from agroapi_sdk import *

def	main():

# The purpose of this program is to simulate paddy rice growth ( Japanese variety )
# at Yokota Farm and a farm in Kyushu
# In the year from 1980 to 2017, Planting date from 1 Apr to 30 June using NARO1km weather data
# 
# The ouput is csv at each variety that consists of
# Farmname, Variety, weatherfile, transplanting date, flowering date, maturity date, yield ( brown-rice after screening), and yield ( rough rice ), and remarks
# file is saved as e.g. YokotaFarm_Koshihikari.csv and YokotaFarm_Koshihikari.json
#
#
# R code from Chen san, there is no tech_coef
#     Conversion factors from rough to brown rice, and panicle to rough rice
#    CVBP = 0.76
#    CVPP = 0.9
#    Terminal Section of  Simulation
#    PYBROD <- DWGRAIN/100.0 : Dry Weight of Grain
#    PYBRON <- PYBROD/0.85   -> Yield Brown Rice at 15% Moisture
#    PYPADY <- PYBRON/CVBP   -> Brown Rice / 0.76
#    PANDW <- PYBROD/CVBP/CVPP  -> Panicle Rice Dry Weight
#
# Java code from NARO Moist is changed to 15%
#
# SimriwData.java:    setValue(TECH_COEF, 0.75);
#
# SimriwElement.java
#  /** [value] Panicle Dry Weight (t/ha) */
#  PANDW,
#  /** [value] Potential Brown Rice in Dry (t/ha) */
#  PYBROD,
#  /** [value] Potential Brown Rice in 14% -> 15% Moist. (t/ha) */
#  PYBRON,
#  /** [value] Potential Rough Rice in 14% -> 15% Moist. (t/ha) */
#  PYPADY,
#  /** [value] Actual Brown Rice in Dry (t/ha) */
#  AYBROD,
#  /** [value] Actual Brown Rice in 14% ->15% Moist. (t/ha) */
#  AYBRON,
#  /** [value] Actual Rough Rice in 14% ->15% Moist. (t/ha) */
#  AYPADY;

# Simriw_.java
# 32   /** conversion factors from rough to brown rice, and panicle to rough rice */
# 33   private double cvbp = 0.76;
# 34   private double cvpp = 0.9;

#206       dw += growthDryWeight(ra, dvi, lai);
#207       data.getTimeSeries(DW).set(interval, dw);
#208 
#209       spikeletSterilityCool(at, dvi, cool);
#210       spikeletSterilityHeat(xt, dvi, heat);
#211       hi = harvestIndex(dvi, cool[0], heat[0], cool[1], heat[1]);
#212       data.getTimeSeries(HI).set(interval, hi);
#213       gy = dw * hi;
#214       data.getTimeSeries(GY).set(interval, gy);
#215 
#216       py = gy / (cvbp * cvpp);
#217       data.getTimeSeries(PY).set(interval, py);
#218 
#219       if(dvi >= data.getValue(DVI_MATURITY)){
#220         data.getTimeSeries(DVI).set(interval, data.getValue(DVI_MATURITY));
#221 
#222         double pybrod = gy / 100;
#223         data.setValue(PYBROD, pybrod);
#224 
#225         double pybron = pybrod / 0.86;
#226         data.setValue(PYBRON, pybron);
#227 
#228         double pypady = pybron / cvbp;
#229         data.setValue(PYPADY, pypady);
#230 
#231         double tech_coef = data.getValue(TECH_COEF);
#232         data.setValue(AYBROD, pybrod * tech_coef);
#233         data.setValue(AYBRON, pybron * tech_coef);
#234         data.setValue(AYPADY, pypady * tech_coef);
#235         data.setValue(PANDW, pybrod / (cvbp * cvpp));
#236         data.setValue(DWT, dw / 100);
#237       }
#	gw grin dry weight[kg/ha] -> py potential yield (before cvbp and dvpp )
#	gw -> pybrod


################################
#	Yokota Farm Data

	lat = 35.918816
	lon = 140.240321
	farm = 'YokotaFarm'
	wtdFBase = 'YOKT'


#	Get varieties to simulate
#	varieties = get_varieties_simriw()
#	for testing
	varieties = ['Koshihikari']

#	weather_properties = get_required_weather_variables_simriw()
	weather_properties = [	
			"daily_average_air_temperature",
			"daily_maximum_air_temperature",
			"daily_minimum_air_temperature",
#			"daily_average_relative_humidity",
#			"daily_sunshine_duration",
			"daily_global_solar_radiant_exposure",
#			"daily_downward_longwave_radiant_exposure",
			"daily_precipitation",
#			"daily_average_wind_speed"
	]


#	To get WTD for Whole Period from naro1km
	weather_year_start=1980
	weather_year_end=2017

#	Transplanting dates window
	year_start = 2017
#	year_start = 1980
	year_end = 2017

	month_start =  4
	dayOfMonth_start =   1
	month_end   =  6
	dayOfMonth_end   = 30 
#	dayOfMonth_end   = 30

##		get naro1km for whole period 
	omfName = farm+'_weather.xml'
	if 0:
		try:
			daybegin=datetime.date(weather_year_start, 1, 1)
			dayend  =datetime.date(weather_year_end  ,12,31)
			wth_om = daily_weather( lat, lon, daybegin, dayend, weather_properties, source='naro1km' )
		except:
			e = sys.exc_info()[0]
			print( 'get_naro1km error:', e)
			raise
		omf = open(omfName, 'w')
		omf.write(wth_om.decode("utf-8"))
		omf.close()
#		om_url = upload( omfName )
#		cannot upload from my pc, so upload manualy
		sys.exit()

##		convert om to wtd format, give extension WTDE, zip and upload
	wtdFName = wtdFBase + '.WTDE'
	if 0:
		om_url = 'http://tesla2.isc.chubu.ac.jp/tmp/'+farm+'/'+omfName
		wtd = om2wtd( om_url )
		wtdf = open(wtdFName, 'w')
		wtdf.write(wtd.decode("utf-8"))
		wtdf.close()
		print( 'generated:',wtdFName )
#		manually zip and upload 
		sys.exit()
	wth_zip = wtdFName + '.zip'
	wth_zip_url = 'http://tesla2.isc.chubu.ac.jp/tmp/'+farm+'/'+wth_zip

	om_urls = []
	wtd_urls=[]
## 	Variety Loop
	for variety in varieties:
#	csv file is by each farm and variety
		try:
			fName = '{}_{}.csv'.format(farm,variety)
			f = open(fName, 'w' )
		except Exception as e:
			e = sys.exc_info()[0]
			print( 'file opern error:%s'%fName, e)
			raise
		fNameJson = '{}_{}.json'.format(farm,variety)
		fj = open(fNameJson, 'w' )
##	Year Loop to make transplanting date array
		transplant_dates=[]
		transplant_dates_str=[]
		for year in range( year_start, year_end+1 ):
	
			transplant_date_start = datetime.date(year,month_start, dayOfMonth_start)
			transplant_date_end   = datetime.date(year,month_end,   dayOfMonth_end  )
			d = transplant_date_start
			while d <= transplant_date_end:
				transplant_dates.append( d )
				transplant_dates_str.append( d.strftime('%Y-%m-%d') )
				d += datetime.timedelta( days=1)
			print( 'constructed date array ', transplant_date_start, transplant_date_end)
#			print( transplant_dates_str )

##		get naro1km for 1 year
#			if 0:
#				try:
#					daybegin=datetime.date(year, 1, 1)
#					dayend  =datetime.date(year,12,31)
#					wth_om = daily_weather( lat, lon, daybegin, dayend, weather_properties, source='naro1km' )
#				except:
#					e = sys.exc_info()[0]
#					print( 'get_naro1km error:', e)
#					raise
##				print('wth_om=', wth_om)
#		
##		save O&M document
#				omfName = farm+'_weather_{}'.format(year)+'.xml'
#				omf = open(omfName, 'w')
#				omf.write(wth_om.decode("utf-8"))
#				omf.close()
##		upload O&M document
#				om_url = file_upload( omfName )

#			convert OM to WTD
#			if 0:
#				omfName = farm+'_weather_{}'.format(year)+'.xml'
#				om_url = 'http://tesla2.isc.chubu.ac.jp/tmp/'+farm+'/'+omfName
#				om_urls.append(om_url)
#				wtd = om2wtd( om_url )
#				wtdFName = wtdFBase + '{}'.format(year)+'.WTDE'
#				wtdf = open(wtdFName, 'w')
#				wtdf.write(wtd.decode("utf-8"))
#				wtdf.close()
#				print( 'generated:',wtdFName )
##			if 0:
#				zipfile = wtdFBase+'.zip'
#				zipFile = 'YOKT2yr.zip'

			
#			Change Extention from WTD to WTDE
#			wth_wtde = rename_wtd2wtde( wtd_wtd ) 
#			zip WTDE
#			wth_wtde_zip = zip( wtd_wtde ) 
#			upload WTDE
#			wth_url = file_upload( wth_wtde_zip )
#			execute simulation
#			simriw( wth_url, pdates, lat, lon, variety )

##	Peform simriw simulation and get simulation id and result in json including links to detail result
		sim_id, sim_result = 		\
			simriw( wth_zip_url, transplant_dates_str, lat, lon, variety  )
		if( sim_id is None ):
			print("simulatino failed:", sim_result )
			sys.exit()

		json.dump( sim_result, fj )
		fj.close()

##	Parsing each run result
		series_set_url = sim_result['json_output']
		series = jsonread( series_set_url )
		idxx = 0
		for line in series:
			if( idxx == 0 ):
				f.write( '{},{},{},{},{},{},{},{},{}\r\n'.format('farm','variety',line[0],line[5],line[1],line[2],'yield_brown_rice(kg/ha)', 'yield_rough_rice(kg/ha)',line[6]))
				idxx += 1
			else:
#				f.write( '{},{},{},{},{},{},{},{},{}\r\n'.format(farm,variety,line[0],line[5],line[1],line[2],line[3],line[4],line[6]))
				f.write( '{},{},{},'.format(farm, variety,line[0]))
				if( line[5] == 'null' ):
					f.write(',')
				else:
					f.write('{},'.format(line[5]))
				if( line[1] == 'null' ):
					f.write(',')
				else:
					f.write('{},'.format(line[1]))
				if( line[2] == 'null' ):
					f.write(',')
				else:
					f.write('{},'.format(line[2]))
				if( line[3] == 'null' ):
					f.write(',')
				else:
					f.write('{},'.format(line[3]))
				if( line[4] == 'null' ):
					f.write(',')
				else:
					f.write('{},'.format(line[4]))
				if( len(line[6]) == 0 ):
					f.write('{}\r\n'.format('\"success\"'))
				else:
					f.write('{}\r\n'.format(line[6]))
			
		f.close()
		print('succesfully simulated: for farm: {}  variety:{}: generated {} and {}'.format(farm, variety, fName, fNameJson ))

def get_varieties_simriw():
	varieties = [
#		"ir36",
#		"ir58",
#		"ir64",
		"ishikari",
		"koshihikari",
		"mizuho",
		"nipponbare",
		"sasanishiki"
	]

if __name__ == "__main__":
	main()
