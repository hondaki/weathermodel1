import json
import datetime, time
import sys
import copy

from agroapi_sdk import *

def main():

	lat=35.7062
	lon = 140.4824
	variety = 'Koshihikari'
	scenario_num = 100
	scenario_id, scenario_result, scenario_link = \
		wgen_pdisag( lat=35.7062, lon = 140.4824, from_date="2017-01-01", to_date = "2017-12-31", scenario_num=scenario_num, \
					source = 'naro1km' )

	print( scenario_result )
	print( 'weather scenario id:', scenario_id, scenario_link )

	transplanting_date = datetime.date(2017,5,8)
	sim_id, sim_result = 		\
		simriw( scenario_link, transplanting_date, lat, lon, variety )

	print( sim_result)

#	return

	pdates=[]
	pdate0 = datetime.date(2017,4,30)
	pdates.append( pdate0 + datetime.timedelta(days = -1*7) )
	pdates.append( pdate0 + datetime.timedelta(days = -1*6) )
	pdates.append( pdate0 + datetime.timedelta(days = -1*5) )
	pdates.append( pdate0 + datetime.timedelta(days = -1*4) )
	pdates.append( pdate0 + datetime.timedelta(days = -1*3) )
	pdates.append( pdate0 + datetime.timedelta(days = -1*2) )
	pdates.append( pdate0 + datetime.timedelta(days = -1*1) )
	pdates.append( pdate0                              )
	pdates.append( pdate0 + datetime.timedelta(days =  1*1) )
	pdates.append( pdate0 + datetime.timedelta(days =  1*2) )
	pdates.append( pdate0 + datetime.timedelta(days =  1*3) )
	pdates.append( pdate0 + datetime.timedelta(days =  1*4) )
#	pdates.append( pdate0 + datetime.timedelta(days = -7*1) )
#	pdates.append( pdate0                              )
#	pdates.append( pdate0 + datetime.timedelta(days =  7*1) )
#	pdates.append( pdate0 + datetime.timedelta(days =  7*2) )
#	
	print( pdates )

	if 1:
		sim_ids =[]
		sim_results=[]
		for pdate in pdates:
			print('calling simriw', pdate )
			sim_id_wk, sim_result_wk = simriw( scenario_link, pdate, lat, lon, variety )
			sim_ids.append( sim_id_wk )
			sim_results.append( sim_result_wk )

		print('simulation done' )

		idx = 0
		for pdate in pdates:
			print(sim_ids[idx], sim_results[idx]['status'] ) 
			idx = idx+1
	else:

# [datetime.date(2017, 4, 17), datetime.date(2017, 4, 24), datetime.date(2017, 5, 1), datetime.date(2017, 5, 8), datetime.date(2017, 5, 15)]

		sim_results=[
{"status":"Crop simulation succeed.","id":"2017-11-19T14:53:16.543Z011eb08580b1e6ab","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:16.543Z011eb08580b1e6ab","model":"simriw","planting_date":None,"transplant_date":"2017-04-24","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:16.543Z011eb08580b1e6ab/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:16.543Z011eb08580b1e6ab/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:16.543Z011eb08580b1e6ab/results/json"},
{"status":"Crop simulation succeed.","id":"2017-11-19T14:53:31.353Z4efd4c153b0e8e1d","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:31.353Z4efd4c153b0e8e1d","model":"simriw","planting_date":None,"transplant_date":"2017-05-01","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:31.353Z4efd4c153b0e8e1d/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:31.353Z4efd4c153b0e8e1d/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:31.353Z4efd4c153b0e8e1d/results/json"},
{"status":"Crop simulation succeed.","id":"2017-11-19T14:53:47.536Z4f8a631303d62982","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:47.536Z4f8a631303d62982","model":"simriw","planting_date":None,"transplant_date":"2017-05-08","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:47.536Z4f8a631303d62982/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:47.536Z4f8a631303d62982/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:53:47.536Z4f8a631303d62982/results/json"},
{"status":"Crop simulation succeed.","id":"2017-11-19T14:54:02.478Zeac51b05d60f94b2","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:02.478Zeac51b05d60f94b2","model":"simriw","planting_date":None,"transplant_date":"2017-05-15","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:02.478Zeac51b05d60f94b2/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:02.478Zeac51b05d60f94b2/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:02.478Zeac51b05d60f94b2/results/json"},
{"status":"Crop simulation succeed.","id":"2017-11-19T14:54:18.436Z3b08fbad9be300fe","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:18.436Z3b08fbad9be300fe","model":"simriw","planting_date":None,"transplant_date":"2017-05-22","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:18.436Z3b08fbad9be300fe/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:18.436Z3b08fbad9be300fe/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:54:18.436Z3b08fbad9be300fe/results/json"},
]

#		sim_results=[
#{"status":"Crop simulation succeed.","id":"2017-11-19T14:11:14.080Z5038bd746e7d7630","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:14.080Z5038bd746e7d7630","model":"simriw","planting_date":None,"transplant_date":"2017-04-22","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:14.080Z5038bd746e7d7630/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:14.080Z5038bd746e7d7630/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:14.080Z5038bd746e7d7630/results/json"},
#{"status":"Crop simulation succeed.","id":"2017-11-19T14:11:29.421Zf3028323c909a6a1","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:29.421Zf3028323c909a6a1","model":"simriw","planting_date":None,"transplant_date":"2017-04-29","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:29.421Zf3028323c909a6a1/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:29.421Zf3028323c909a6a1/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:29.421Zf3028323c909a6a1/results/json"},
#{"status":"Crop simulation succeed.","id":"2017-11-19T14:11:44.405Z6ff61758852acba0","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:44.405Z6ff61758852acba0","model":"simriw","planting_date":None,"transplant_date":"2017-05-06","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:44.405Z6ff61758852acba0/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:44.405Z6ff61758852acba0/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:11:44.405Z6ff61758852acba0/results/json"},
#{"status":"Crop simulation succeed.","id":"2017-11-19T14:12:00.696Z4651cac30abac6ba","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:00.696Z4651cac30abac6ba","model":"simriw","planting_date":None,"transplant_date":"2017-05-13","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:00.696Z4651cac30abac6ba/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:00.696Z4651cac30abac6ba/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:00.696Z4651cac30abac6ba/results/json"},
#{"status":"Crop simulation succeed.","id":"2017-11-19T14:12:17.936Z0bce0aaff71167d6","_self":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:17.936Z0bce0aaff71167d6","model":"simriw","planting_date":None,"transplant_date":"2017-05-20","crop_ident_ICASA":"RIC","cultivar_name":"koshihikari","weather_file":"http://dev.listenfield.com:8080/weather/static/ex/20171119-065629-DGGFoB.zip","raw_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:17.936Z0bce0aaff71167d6/results/raw","csv_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:17.936Z0bce0aaff71167d6/results/csv","json_output":"http://dev.listenfield.com:3232/cropsim/v1.1/simulations/2017-11-19T14:12:17.936Z0bce0aaff71167d6/results/json"},
#		]

	boxplot_template = {
			"dataName":"flowering date",
			"direction":"h",
			"vtype":"date",
			"vmin":"2017-04-01",
			"vmax":"2017-11-01",
			"size":[30,4],
			"dpi":200,
			"categories":[],
			"dataset":[]
	}
	series_template= {
			"category":None, "data":[]
	}

	categories = []
	dataset=[]
	boxplot_flowering = copy.deepcopy(boxplot_template)
	boxplot_flowering['dataName'] = 'Flowering Date'
	boxplot_flowering['vtype'] = 'date'
	boxplot_flowering['direction'] = 'h'

	boxplot_maturity  = copy.deepcopy(boxplot_template)
	boxplot_maturity ['dataName'] = 'Maturity Date'
	boxplot_maturity['vtype'] = 'date'
	boxplot_maturity['direction'] = 'h'

	boxplot_yield     = copy.deepcopy(boxplot_template)
	boxplot_yield    ['dataName'] = 'Yield [kg/ha]'
	boxplot_yield['vtype'] = 'float'
	boxplot_yield['direction'] = 'v'
	boxplot_yield['size'] = [20,10] 

	idx=0
	yldmin = sys.float_info.max
	yldmax = sys.float_info.min

	flwmin = None
	flwmax = None
	matmin = None
	matmax = None
	for sim_result in sim_results:
		pdate = datetime.datetime.strptime(sim_result['transplant_date'],'%Y-%m-%d' ).date()
		series_set_url = sim_result['json_output']
#		print( pdate, series_set_url )
		series = jsonread( series_set_url )
#		print( series )

		pdate_str = pdate.strftime('%-m/%d')
		flowering_dates = copy.deepcopy(series_template)
		flowering_dates['category'] = pdate_str
		boxplot_flowering['categories'].append( pdate_str )

		maturity_dates  = copy.deepcopy(series_template)
		maturity_dates['category'] = pdate_str
		boxplot_maturity['categories'].append( pdate_str )

		yields          = copy.deepcopy(series_template)
		yields        ['category'] = pdate_str
		boxplot_yield['categories'].append( pdate_str )

		idxx = 0
#		print(len(series))
		nd = len(series)
		nfail=0
		for line in series:
#			print( idxx, line )
			if( idxx == 0 ):
				idxx = idxx+1
				continue
			idxx = idxx+1
			if( line[1] != 'null' ):
				flowering_dates['data'].append(datetime.datetime.strptime(line[1],'%Y-%m-%d' ).date() )
			if( line[2] != 'null' ):
				maturity_dates['data'].append( datetime.datetime.strptime(line[2],'%Y-%m-%d' ).date() )
			if( line[3] != 'null' ):
				yields['data'].append(                                    line[3]                     )
				yld = float(line[3])
			else:
				nfail += 1
				yields['data'].append(                                    0.                     )
				yld = 0.
			
			if( yld < yldmin ):
				yldmin = yld
			if( yld > yldmax ):
				yldmax = yld
#			print( yld, yldmax )
		if( nd < scenario_num ):
			n2add = int((scenario_num - nd))
			for _ in range(n2add):
				yields['data'].append( 0. )
			yldmin = 0.
		print('nd = ',nd, 'nfail=',nfail)

		boxplot_flowering['dataset'].append( copy.deepcopy(flowering_dates))
		boxplot_maturity ['dataset'].append( copy.deepcopy(maturity_dates))
		boxplot_yield    ['dataset'].append( copy.deepcopy(yields))
		boxplot_flowering['vmin'] = datetime.date(2017,7,1)
		boxplot_flowering['vmax'] = datetime.date(2017,10,1)
		boxplot_maturity['vmin'] = datetime.date(2017,8,1)
		boxplot_maturity['vmax'] = datetime.date(2017,11,1)


	boxplot_flowering['categories'].reverse()
	boxplot_maturity['categories'].reverse()
#	nSimulation = len(sim_results)
#	for i in range(int(nSimulation/2)):
#		tmp = boxplot_flowering['dataset'][i]
#		boxplot_flowering['dataset'] = boxplot_flowering['dataset'][nSimulation-i-1]
#		boxplot_flowering['dataset'][nSimulation-i-1] = tmp

	yldmin = int(yldmin/1000)*1000.
	yldmax = (int(yldmax/1000)+1)*1000.
	boxplot_yield['vmin'] = yldmin
	boxplot_yield['vmax'] = yldmax

#	boxplot_flowering_payload = json_dumps(boxplot_flowering)
#	boxplot_maturity_payload  = json_dumps(boxplot_maturity)
#	boxplot_yield_payload     = json_dumps(boxplot_yield)

	genBoxplot( boxplot_flowering, 'boxplot_flowering.png' )
	genBoxplot( boxplot_maturity , 'boxplot_flowering.png' )
	genBoxplot( boxplot_yield    , 'boxplot_flowering.png' )

#	print(boxplot_flowering)
#	print(boxplot_maturity)
#	print(boxplot_yield)

#	print(boxplot_flowering_payload, 'boxplot_flowering.png')
#	print(boxplot_maturity_payload, 'boxplot_maturity.png')
#	print(boxplot_yield_payload, 'boxplot_yield.png')

#		print( sim_result )
#		print(data['transplant_date'] )
#		print( series )


if __name__ == "__main__":
	main()

'''
simulation done
2017-11-19T07:48:09.296Z010211cf93c1be40 Crop simulation succeed.
2017-11-19T07:48:23.073Z1741f0f5bc9a11cd Crop simulation succeed.
2017-11-19T07:48:37.182Z5552c3d556c311ea Crop simulation succeed.
2017-11-19T07:48:52.776Z59972e424fdc35ae Crop simulation succeed.
2017-11-19T07:49:08.471Z3d21b79a66222920 Crop simulation succeed.
'''
