import json
import http.client
import datetime, time
import sys
import urllib.request
import copy
from urllib.parse import quote

def	json_dumps( js ):
	JSONEncoder_olddefault = json.JSONEncoder.default
	def JSONEncoder_newdefault(self, o):
#		if isinstance(o, UUID): return str(o)
		if isinstance(o, datetime.datetime): return str(o)
		if isinstance(o, datetime.date): return o.strftime('%Y-%m-%d')
		if isinstance(o, time.struct_time): return datetime.fromtimestamp(time.mktime(o))
#		if isinstance(o, decimal.Decimal): return str(o)
		if isinstance(o, decimal.Decimal): return float(str(o))
		return JSONEncoder_olddefault(self, o)

	json.JSONEncoder.default = JSONEncoder_newdefault

	return json.dumps(js)


def wgen_pdisag( source="naro1km", lat=35.706179, lon = 140.482362, from_date="2017-01-01", to_date = "2017-12-31", scenario_num=100 ):

	conn = http.client.HTTPConnection("dev.listenfield.com")
	
	payload_json = {
		"wth_src"		: "naro1km",
		"wgen_model"	: "pdisagws",
		"scenario_num"	: "100",
		"latitude"		: "35.706179",
		"longitude"		: "140.482362",
		"from_date"		: "2017-01-01",
		"to_date"		: "2017-12-31",
		"bn_nn_an"		: "33:34:33",
		"monthly_adjust": "true"
	}

#	dt = datetime.date(2017,5,3)
#	dtstr=dt.strftime('%Y-%m-%d')
#	payload_json['from_date']=dtstr

	payload_json['latitude']=lat
	payload_json['longitude']=lon
	payload_json['from_date']=from_date
	payload_json['to_date']=to_date
	payload_json['scenario_num']=scenario_num
	payload_json['wth_src']= source
	
	
	payload  = json_dumps(payload_json)
	print( payload )
	
	headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "1cef0e9c-e5ac-a840-e7d2-f05c95bd2a38"
    }

	conn.request("POST", "/weather/generator/v1.1/scenarios", payload, headers)
	
	res = conn.getresponse()
	data = res.read()
	data_str = (data.decode("utf-8"))
#	print(data_str)
	
	scenario_json = json.loads(data.decode("utf-8"))
	scenario_link = scenario_json['result']['downloadLink']
	scenario_id = scenario_json['ID']

	return scenario_id, scenario_json, scenario_link
	


def simriw( wth_files_zipped_url, pdate, lat, lon, variety ):
	conn = http.client.HTTPConnection("dev.listenfield.com")


	payload_json = {
		'model': 'simriw',
		'crop_ident_ICASA': 'RIC',
		'cultivar_name': 'Koshihikari',
		'transplant_date': '2017-05-26',
		'field_latitude': 35.706179,
		'field_longitude': 140.482362,
		'weather_file': 'http://dev.listenfield.com/weather/static/ex/20171119-055606-zQmCww.zip',
		'wait': True,
	}

	headers = {
	    'content-type': "application/json",
	    'cache-control': "no-cache",
	    'postman-token': "c10b5c00-ce7c-0566-842e-85417082cb31"
	    }

	payload_json['cultivar_name'] = variety
#	print('pdate=',pdate)
	if( isinstance(pdate,list) ):
		payload_json['transplant_date'] = pdate
	else:
		payload_json['transplant_date'] = pdate.strftime('%Y-%m-%d')
	payload_json['weather_file'] = wth_files_zipped_url
	payload_json['field_latitutde'] = lat
	payload_json['field_longitude'] = lon

	payload = json_dumps( payload_json )
	print( payload )

	conn.request("POST", "/cropsim/v1.1/simulations", payload, headers)

	res = conn.getresponse()
	data = res.read()
	data_str = (data.decode("utf-8"))
	print(data_str)

	simresult_json = json.loads(data.decode("utf-8"))
	if( 'id' in simresult_json.keys() ):
		simresult_id = simresult_json['id']
		return simresult_id, simresult_json
	else:
		print( simresult_json )
		return None, simresult_json

#		print( series )


def jsonread( url ):
	try:
		res = urllib.request.urlopen( url )
		data = json.loads( res.read().decode('utf-8') )
	except urllib.error.HTTPError as e:
		print ('HTTPError', e)
		sys.exit()
	except json.JSONDecodeError as e:
		print ('JSONDecodeError', e)
		sys.exit()

	return data


# request daily weahter data at lat,lon from the source 
def daily_weather( lat, lon, startdate, enddate, properties, source='naro1km' ):

# curl and http sample
#curl -X GET --header 'Accept: text/xml' 'http://133.130.90.193/api/sos/getobservation/urn%3ANARO%3AAgroWeatherMesh?props=daily_global_solar_radiant_exposure%2Cdaily_maximum_air_temperature%2Cdaily_minimum_air_temperature%2Cdaily_precipitation%2Cdaily_average_air_temperature&begin=2017-01-01T00%3A00%3A00.00Z&end=2017-01-30T00%3A00%3A00.00Z&lower=36.3333%20140.4444&upper=36.3333%20140.4444&cache=true&sos=true&external=false'
# http://133.130.90.193/api/sos/getobservation/urn%3ANARO%3AAgroWeatherMesh?props=daily_global_solar_radiant_exposure%2Cdaily_maximum_air_temperature%2Cdaily_minimum_air_temperature%2Cdaily_precipitation%2Cdaily_average_air_temperature&begin=2017-01-01T00%3A00%3A00.00Z&end=2017-01-30T00%3A00%3A00.00Z&lower=36.3333%20140.4444&upper=36.3333%20140.4444&cache=true&sos=true&external=false

	if( source != 'naro1km' ):
		return '<error> this suporce is not supported</error>'

	server = "133.130.90.193"
	conn = http.client.HTTPConnection(server)

	headers = {
#		'cache-control': "no-cache",
		'Accept': "text/xml",
		'postman-token': "42ff70f7-421b-12d9-d23f-49e76f88821d"
	}

	propsStr = 'props=' + quote(','.join(properties))
	beginStr = 'begin=' + quote(startdate.strftime('%Y-%m-%dT00:00:00.00Z'))
	endStr   = 'end='   + quote(enddate.strftime('%Y-%m-%dT00:00:00.00Z'))
	lowerStr = 'lower=' + quote('{} {}'.format(lat,lon))
	upperStr = 'upper=' + quote('{} {}'.format(lat,lon))
	req = "/api/sos/getobservation/urn%3ANARO%3AAgroWeatherMesh?"+propsStr+'&'+beginStr+'&'+endStr + '&'+lowerStr + '&' + upperStr + '&cache=true&sos=true&external=false'

	print( ' naro1km: requesting: ', req )
	
	conn.request("GET", req, headers=headers)

	res = conn.getresponse()
	data = res.read()

#	print(data.decode("utf-8"))
	return data


def genBoxplot(command, baseName ):

	conn = http.client.HTTPConnection("api.listenfield.com:80")
	
#	dt = datetime.date(2017,5,3)
#	dtstr=dt.strftime('%Y-%m-%d')
#	payload_json['from_date']=dtstr
	
	payload  = json_dumps(command)
	print( payload )
	
	headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    }

	conn.request("POST", "/cgi-bin/boxplot_cgi.py", payload, headers)
	
	res = conn.getresponse()
	data = res.read()
	data_str = (data.decode("utf-8"))
#	print(data_str)
	
	bbox_json = json.loads(data.decode("utf-8"))
#	print( bbox_json )
	print( bbox_json['boxplot_link'] )

	pass

def file_upload( fName, content_type='text' ): 

	conn = http.client.HTTPConnection("dev.listenfield.com")

	payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"upload_file\"; filename=\"{}\"\r\nContent-Type: application/{}\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--".format(fName, content_type)

#	payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"upload_file\"; filename=\"simulate.py\"\r\nContent-Type: text\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"

	print( payload )
	
	headers = {
	'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
	'cache-control': "no-cache",
	'postman-token': "78f9bf03-1f32-2f55-fe16-a81c02e2db47"
	}
	conn.request("POST", "/cropsim/v1.1/uploads", payload, headers)

	res = conn.getresponse()
	data = res.read()

	print(data.decode("utf-8"))


def om2wtd( om ):

	conn = http.client.HTTPConnection("ec2-52-69-188-223.ap-northeast-1.compute.amazonaws.com:8080")
	
	headers = {
		'cache-control': "no-cache",
		'postman-token': "a46b1d84-d79a-d927-3f50-a2fc2acc67ba"
	}

	omesc = quote(om)
	conn.request("GET", "/DataTransformation/rest/transformation?url="+omesc, headers=headers)

	res = conn.getresponse()
	data = res.read()
#	print(data.decode("utf-8"))
	return data



if __name__ == "__main__":
	if 0:
		lat = 35.918816
		lon = 140.240321
		startDate = datetime.date(2017,1,1)
		endDate = datetime.date(2017,12,31)
		weather_variables = [	
			"daily_average_air_temperature",
			"daily_maximum_air_temperature",
		]
		naro1km(lat,lon,startDate, endDate,weather_variables)
	if 0:
		file_upload('YokotaFarm_weather_2016.xml', 'xml')

	if 1:
		om2wtd( 'http://tesla2.isc.chubu.ac.jp/tmp/yokotafarm/YokotaFarm_weather_2017.xml' )
