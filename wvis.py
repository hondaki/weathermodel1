import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.dates import MonthLocator, DateFormatter, WeekdayLocator
import pandas as pd
#import urllib2 as ul
import urllib3 as ul
#import urlparse
from urllib.parse import urlparse
import copy
import datetime
from xml.dom import minidom
import os.path
import json

# It reads wtd files from local/http and visualize
# It shows mean ( or median.. pleas edit ), max, min, ptile67 and ptile33

class dailyWScenario:
	columns = 0
	fromYear = 0
	fromDoy= 0
	fromYearDoy = 0
	toYear = 0
	toDoy= 0
	toYearDoy = 0
#	fromDate = datetime.date( datetime.MINYEAR,1,1)
#	toDate = fromDate

	ndvalue = -99.0
	dateName = ['date', 'DATE']
	sradName = ['daily_solar_irradiance','SRAD']
	tmaxName =['daily_maximum_temperature', 'TMAX']
	tminName =['daily_maximum_temperature', 'TMIN']
	rainName =['daily_precipitation', 'RAIN']

#	columns = ['DATE','SRAD','TMAX','TMIN','RAIN']
#	uoms = ['YYYYDOY','MJm-2','Cel','Cel','mm']
#	df = pd.DataFrame(columns = columns ) 

	def __init__(self, url = [], verbose = False ):
		if( len(url) ):
			self.read( url, verbose = verbose  )
		pass

	def append( dayData ):
		pass


	def	read( self, url, verbose = False ):

		self.verbose = verbose
#		if( is_url( url ) ):
#			try:
#				print "opening url"
#				f = ul.urlopen( url )
#			except:
#				print( "opening url error [%s]"%url )
#			self.url = url
#			self.ifurl = True
#		else:
#			try:
#				print "opening a file"
#				f = open( url, "r" )
#			except:
#				print "opening file error"
#			self.url = url
#			self.ifurl = False
#
#		print f
#		header = f.readline()
#		data = np.genfromtxt( f, dtyp = float, delimiter=[7,6,6,6,6,6,6,6,6], names=True )
#		print data.dtype.names
#
#		f.close()
#	
#		print "header [%s]"%header
#
#		self.df = pd.read_csv( url, delim_whitespace=True, skiprows=1 )
#	WTD file has fixed column position YYYYDOY + n(f6.x)
#		self.df = pd.read_fwf( url, colspec=[7,6,6,6,6,6,6,6,6,6], dtype={0:np.integer} )
		self.df = pd.read_fwf( url, colspec=[7,6,6,6,6,6,6,6,6,6] )
		print( 'reading: %s'%url )
		if verbose:
			print( self.df.columns )
#		for name in self.df.columns:
#			print name
		self.df = self.df.rename_axis( { self.df.columns[0] : "DATE"}, axis = "columns"  )
#		fromYearDoy = int(self.df['DATE'][0])
		self.fromYearDoy = int(self.df.iloc[0]['DATE'])
		self.fromYear = self.fromYearDoy/1000
		self.fromDoy = self.fromYearDoy - self.fromYear*1000
		self.toYearDoy = int(self.df.iloc[-1]['DATE'])
		self.toYear = self.toYearDoy/1000
		self.toDoy = self.toYearDoy - self.toYear*1000
		if verbose:
			print( self.fromYearDoy, self.fromYear, self.fromDoy )
			print( self.toYearDoy, self.toYear, self.toDoy )
		pass

	def	readMulti( urls ):
		pass

	def	addData():
		pass

	def write( file ):
		pass

class dailyWScenarios:
	scenario = []
	fromYearDoy = 0
	fromYear = 0
	fromDoy = 0
	toYearDoy = 0
	toYear = 0
	toDoy = 0
	samePeriod = True
	urls = []

	mean = dailyWScenario()
	median = dailyWScenario()
	minimum = dailyWScenario()
	maximum = dailyWScenario()
	stdev = dailyWScenario()
	ptile33 = dailyWScenario()
	ptile67 = dailyWScenario()

	verbose = False

	def __init__( self, urls = [], samePeriod = True ):
		self.samePeriod = samePeriod
		if( len( urls ) ):
			self.append( urls )
		pass

	def	stat( self ):
#		print self.scenario[0].df['DATE']
#		sc = self.scenario[0]
#		print "original sc.df.ix[0]['SRAD'] = %f"%(sc.df.loc[0,'SRAD'])
#		sc = self.scenario[1]
#		print "original sc.df.ix[0]['SRAD'] = %f"%(sc.df.loc[0,'SRAD'])
#		self.mean.df.iloc[0]['SRAD'] = 100. 
#		self.mean.df.ix[0,'SRAD'] = 20.
		sc = self.scenario[0]
		n = sc.df.shape[0]
		c = sc.df.shape[1]
#		self.mean.df = pd.DataFrame( data = sc.df )
#		self.mean.df = pd.DataFrame( data = sc.df, columns = self.scenario[0].columns  )
#		self.mean = pd.DataFrame( np.empty_like( sc.df.shape ), index =sc.df.index, columns = sc.columns  )
#		print sc.df.shape
		print( "%s: number of data = %d, number of column = %d"%(__file__,n,c) )
		self.mean.df = pd.DataFrame( np.zeros( (n,c)  ), columns = sc.df.columns )
		self.median.df = pd.DataFrame( np.zeros( (n,c)  ), columns = sc.df.columns )
		self.minimum.df = pd.DataFrame( np.zeros( (n,c)  ), columns = sc.df.columns )
		self.maximum.df = pd.DataFrame( np.zeros( (n,c)  ), columns = sc.df.columns )
		self.stdev.df   = pd.DataFrame( np.zeros( (n,c)  ), columns = sc.df.columns )
		self.ptile33.df = pd.DataFrame( np.zeros( (n,c)  ), columns = sc.df.columns )
		self.ptile67.df = pd.DataFrame( np.zeros( (n,c)  ), columns = sc.df.columns )
		self.date 	= pd.DataFrame( np.zeros( n  ), columns = ['date'])
		print( "mean.df.shape=", self.mean.df.shape )
		sc0 = self.scenario[0]
#		for i in xrange(0,n):
		for i in  range(0,n):
			t = sc0.df.ix[i,'DATE']
			yr = int( int( t ) /1000 )
			doy = int(int( t ) - yr*1000)
			print( t,yr,doy )
			self.date.ix[i] = datetime.date(yr,1,1) + datetime.timedelta( days = (doy-1) )
#		print self.date
			
#		self.mean.df.columns = sc.df.columns
#		print self.mean.df
#		print self.mean.df.columns
		sc0 = self.scenario[0]
		n = sc0.df.shape[0]
		m = len(self.scenario)
		pc33 = np.zeros(m)
		pc67 = np.zeros(m)
		day1 = np.zeros(m)
#		print "n=%d m=%d"%(n,m)
		for column in sc0.df.columns:
#			for i in xrange(0,n):
			for i in  range(0,n):
				sum = 0
				vmin = self.scenario[0].df.ix[i, column ]
				vmax = self.scenario[0].df.ix[i, column ]
				var = 0
				mm = 0
				for sc in self.scenario:
#					sum += sc.df.ix[i, column ]
#					vmin = min( vmin, sc.df.ix[i, column ] )
#					vmax = max( vmax, sc.df.ix[i, column ] )
#					sum2 = sc.df.ix[i, column ] * sc.df.ix[i, column ] 
					day1.flat[mm] = sc.df.ix[i, column ]
#					pc33.flat[mm] = sc.df.ix[i, column ]
#					pc67.flat[mm] = sc.df.ix[i, column ]
					mm+=1
				if( self.verbose and i == 0 ):
					print( 'day1=',day1 )
#				self.mean.df.ix[i, column ] = sum/m
				self.mean.df.ix[i, column ] = np.mean(day1)
				self.median.df.ix[i, column ] = np.median(day1)
				self.minimum.df.ix[i, column ] = np.amin(day1)
				self.maximum.df.ix[i, column ] = np.amax(day1)
				self.stdev.df.ix[i, column ] = np.std( day1 )
				self.ptile33.df.ix[i, column ] = np.percentile( day1, int(1./3.*100.) )
				self.ptile67.df.ix[i, column ] = np.percentile( day1, int(2./3.*100.) )
				if( self.verbose and i == 0 ):
					print( self.maximum.df.ix[i, column ] )
					print( self.ptile67.df.ix[i, column ] )
					print( self.mean.df.ix[i, column ] )
					print( self.median.df.ix[i, column ] )
					print( self.ptile33.df.ix[i, column ] )
					print( self.minimum.df.ix[i, column ] )
		pass

	def append( self, urls, verbose = False ):
		for url in urls:
			wsc = dailyWScenario( url, verbose )
			if( len(self.scenario) == 0 ):
				self.fromYear = wsc.fromYear
				self.fromDoy = wsc.fromDoy
				self.fromYearDoy = wsc.fromYearDoy
				self.toYear = wsc.toYear
				self.toDoy = wsc.toDoy
				self.toYearDoy = wsc.toYearDoy
				self.scenario.append( wsc )
				if verbose:
					print( 'new period, added' )
			elif self.samePeriod :
				if( self.fromYearDoy == wsc.fromYearDoy and self.toYearDoy == wsc.toYearDoy ):
					if verbose:
						print( self.fromYearDoy, self.toYearDoy )
						print( wsc.fromYearDoy, wsc.toYearDoy )
					self.scenario.append( wsc )
					if verbose:
						print( 'same period, added' )
				else:
					print( "dailyWScenarios: start Year DOY and to Year Doy does not match: skip" )
			else:
				self.scenario.append( wsc )
				if verbose:
					print( 'anyway added' )
#			print 'len of self.scenario is %d'%len(self.scenario)
		return
		pass


def is_url(url):
	return urlparse.urlparse(url).scheme != ""


"""
	wvis -max -min -mean -median -ptile33 -ptile67 [ *.WTD | --list weatherlist.xml ]
"""
import argparse
def main():
	parser = argparse.ArgumentParser()

	parser.add_argument('-max', default=False, action='store_true', help="plot maximum")
	parser.add_argument('-min', default=False, action='store_true', help="plot minimum")
	parser.add_argument('-mean', default=False, action='store_true', help="plot mean")
	parser.add_argument('-median', default=False, action='store_true', help="plot median")
	parser.add_argument('-ptile33', default=False, action='store_true', help="plot ptile33")
	parser.add_argument('-ptile67', default=False, action='store_true', help="plot ptile67")
	parser.add_argument('-fixscale', default=False, action='store_true', help="use default fix scale")
	parser.add_argument('-nshow', default=False, action='store_true', help="not show to screen")
	parser.add_argument('--list', default=[], nargs=1, help='a file that contains weather data files(urls).')
	parser.add_argument('-verbose', default=False, action='store_true', help="verbose to show progress")
	parser.add_argument('weatherfiles', metavar='weatherfile', nargs='*', help='weather data files(urls).')

	args = parser.parse_args()

	if( not(args.max or args.min or args.mean or args.median or args.ptile33 or args.ptile67 ) ):
#		print( "%s: %s"%(__file__, 'all plot arguments are false' ))
		args.max = True
		args.min = True
		args.mean = True
		args.ptile33 = True
		args.ptile67 = True

	plotMax = args.max
	plotMin = args.min
	plotMean = args.mean
	plotMedian = args.median
	plotPtile33 = args.ptile33
	plotPtile67 = args.ptile67
	verbose = args.verbose
	fixscale = args.fixscale
	nshow = args.nshow

#	print ("length of weatherfile is %d"%( len( args.weatherfiles ) ) )
#	print ( args.weatherfiles )

#	print ("list file %d %s"%( len( args.list), args.list ) )
#	print ( args.list )

	if( len( args.weatherfiles) == 0 and len( args.list) == 0 ):
		parser.print_help()
#		raise Exception( "%s: %s"%(__file__, 'weather file must be specified either by a list( file ) or command arguments'))
		print( "%s: %s"%(__file__, 'weather file must be specified either by a list( file ) or command arguments'))
		sys.exit(1)

	if( len( args.weatherfiles) != 0 and len( args.list) != 0 ):
		parser.print_help()
#		raise Exception( "%s: %s"%( __file__, 'weather file cannot be specified both by a list( file ) and command arguments'))
		print( "%s: %s"%( __file__, 'weather file cannot be specified both by a list( file ) and command arguments'))
		sys.exit(1)

#	print ('calling sys.exit')
#	sys.exit()

	if( len(args.weatherfiles) > 0 ):
		urls = args.weatherfiles
	else:
		urls = []
		print( "args.list =", args.list )
		base, ext = os.path.splitext( args.list[0] )
		print('ext=',ext)
		if( ext == '.xml' ):
			xmldoc = minidom.parse( args.list[0] )
			weatherList = xmldoc.getElementsByTagName('scenario')
			print( "%s: number of weather scenario is %d"%(__file__, len(weatherList)))
			for s in weatherList:
#				print( s.attributes['href'].value)
				url = s.attributes['href'].value
				urls.append( url )
		elif( ext == '.json' ):
			f = open( args.list[0], 'r' )
			docs = json.load(f)
			weatherList =  docs['weatherScenarios']
			for s in weatherList:
				urls.append(s)
	print(urls)
#		sys.exit()
	"""
#	file = args.graph
#	urls1 = []
#	url1 = 'test_BU/COMM0001.WTD'
#	url1 = "http://weatherscenarios.s3-website-ap-northeast-1.amazonaws.com/zukpvlkvvhkqhfncjznn/COMM0001.WTD"
#	url2 = "http://weatherscenarios.s3-website-ap-northeast-1.amazonaws.com/zukpvlkvvhkqhfncjznn/COMM0002.WTD"
#	url1 = "COMM0001.WTD"
#	url2 = "COMM0002.WTD"
#	url3 = "COMM0003.WTD"
#	url4 = "COMM0004.WTD"
#	url5 = "COMM0005.WTD"

#	urls1 = [url1, url2, url3, url4, url5 ]
#	urls1 = [url1]
		"""
#		urls2 = []
##		for i in xrange(0,100):
###		for i in xrange(0,10):
#		for i in  range(0,100):
##		for i in  range(0,10):
##			url = "http://weatherscenarios.s3-website-ap-northeast-1.amazonaws.com/zukpvlkvvhkqhfncjznn/COMM%4.4d.WTD"%(i+1)
#			url = "http://weatherscenarios.s3-website-ap-northeast-1.amazonaws.com/ekhvaqoqwngsuaubolxo/COMM%4.4d.WTD"%(i+1)
#			print url
#			urls2.append( url )
#		urls = urls2

#	print url/
# this WTDE has format erros, and srad is not MJ, but W
#	url2 = "http://weatherscenarios.s3-website-ap-northeast-1.amazonaws.com/cdbfabhjdsyrkroixpos/COMM0001.WTDE"
#	df = pd.read_csv( url, delim_whitespace=True, skiprows=1 )
#	print df

#	urls.append( url1 )
#	urls.append( url2 )
#	wtd = fileWTD( url )

	scenarios = dailyWScenarios()
	scenarios.verbose = args.verbose
	scenarios.append( urls )
	scenarios.stat()
	
	figNline = 4
	fig = plt.figure( "Weather Scenario", figsize=(12,figNline*3.0))
#	fig.suptitle( "Weather Scenario" )
#	x = scenarios.scenario[0].df['DATE']
	x = scenarios.date
#	fig, ax = plt.subplots( figNline,1,figsize=(6,figNline*3.0), sharex=True, sharey=False)
#	fig.add_title( "Weather Scenario")


	graph = 	[
			['RAIN','Daily Precipitation [mm]', 'bar', 0, 350 ],
			['SRAD','Daily Solar Exposure [MJ/m2]', 'line',0,35],
			['TMAX','Daily Max. Temperature [Cel]', 'line',  0, 45], 
			['TMIN','Daily Min. Temperature [Cel]', 'line', -10, 35]
			]

	pos = 1
	for g in graph:
		print( "%s: plotting"%__file__, g[0], g[1] )
		item = g[0]
		dname = g[1]
		type = g[2]
		ylim = (g[3],g[4])
		if( type == 'line' or type == 'bar' ):
			ax = fig.add_subplot(figNline,1,pos)
#			ax = fig.subplot( pos, 1 )
			ax.set_title(dname, fontsize=12)
			ax.set_xlabel(r'date', fontsize=12)
			ax.set_ylabel(dname, fontsize=10)
			start, end = ax.get_xlim()

			if( plotMax ):
				y = scenarios.maximum.df[item]
				ax.plot( x, y, 'bo', lw=0.2, label = "maximum", markersize = 1.0)
			if( plotPtile67 ):
				y = scenarios.ptile67.df[item]
				ax.plot( x, y, 'b-', lw=0.5, label = "ptile67")
			if( plotMean ):
				y = scenarios.mean.df[item]
				ax.plot( x, y, 'r-', lw=1, label = "mean")
			if( plotMedian ):
				y = scenarios.median.df[item]
				if( plotMean ):
					col = 'y-'
				else:
					col = 'r-'
				ax.plot( x, y, col, lw=1, label = "median")
			if( plotPtile33 ):
				y = scenarios.ptile33.df[item]
				ax.plot( x, y, 'g-', lw=0.5, label = "ptile33")
			if( plotMin ):
				y = scenarios.minimum.df[item]
				ax.plot( x, y, 'go', lw=0.2, label = "minimum", markersize = 1.0)

#			mondays = WeekdayLocator(MONDAY)
#			months = MonthLocator(     range(1,13) , bymonthday=1,interval=2)
			months = MonthLocator(list(range(1,13)), bymonthday=1,interval=2)
#			monthsFmt = DateFormatter("%b '%y")
#			ax.xaxis.set_major_locator(months)
			ax.grid(True)
			ax.autoscale_view()
			if( fixscale ):
				ax.set_ylim(ylim)

		else:
			ax = fig.add_subplot(figNline,1,pos, title = dname)
			y = scenarios.maximum.df[item]
			ax.bar( x, y, 1, color='b', label = "maximum")
#			y = scenarios.ptile67.df[item]
#			ax.bar( x, y, 'b-', lw=0.5, label = "ptile67")
#			y = scenarios.mean.df[item]
##			y = scenarios.median.df[item]
#			ax.bar( x, y, 'red', lw=1, label = "mean")
#			y = scenarios.ptile33.df[item]
#			ax.bar( x, y, 'black', lw=0.5, label = "ptile33")
#			y = scenarios.minimum.df[item]
#			ax.bar( x, y, 'b-', lw=0.2, label = "minimum")
			ax.set_xlabel('date')
			ax.set_ylabel(dname)
		plt.legend(loc='best', fontsize = 10, frameon=False)
		pos += 1
	fig.autofmt_xdate()
	fig.tight_layout()
#	fig.autolayout()

#	y = scenarios.maximum.df['SRAD']
#	ax.plot( x, y, 'b-', lw=0.2, label = "SRAD maximum")
#	y = scenarios.ptile67.df['SRAD']
#	ax.plot( x, y, 'b-', lw=0.5, label = "SRAD ptile67")
#	y = scenarios.mean.df['SRAD']
##	y = scenarios.median.df['SRAD']
#	ax.plot( x, y, 'red', lw=1, label = "SRAD mean")
#	y = scenarios.ptile33.df['SRAD']
#	ax.plot( x, y, 'black', lw=0.5, label = "SRAD ptile33")
#	y = scenarios.minimum.df['SRAD']
#	ax.plot( x, y, 'b-', lw=0.2, label = "SRAD minimum")
#	ax.set_xlabel('date')
#	ax.set_ylabel('Solar Irradiance [MJ/m2]')
#	plt.legend(loc='best', fontsize = 10, frameon=False)


	plt.savefig('wvis.png', bbox_inches='tight')	# save the graph as wvis.png
	if( not nshow ):
		plt.show()

#	for sc in scenarios.scenario:
#		print sc.df

main()
