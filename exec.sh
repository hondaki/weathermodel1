#rscript norm.r 0.15 0.25 0.6
#open figr.png
#python norm.py 0.15 0.25 0.6 -show
#python norm.py 0.15 0.25 0.6
#
# show graph on screen, save them as png files
#python gamma.py 0.9 1 0.2 0.3 0.5 -show
python gamma.py 1.2 50 0.2 0.3 0.5 -show
#
# not show graph on secreen but save as png files
#python gamma.py 0.9 1 0.2 0.3 0.5
#python gamma.py 1.2 50 0.2 0.3 0.5
#open gamma_dist.png
#open gamma_hist.png
##open gamma_original.csv
##open gamma_adjusted.csv
python wvis.py -h
python wvis.py  -ptile33 -ptile67 -mean --list CaseStudy1_ListWTD_DisAg.xml -verbose
python wvis.py --list CaseStudy1_ListWTD_DisAg.xml
python wvis.py -median *.WTD -ptile33 -ptile67
python wvis.py --list wgen_result.json
python wvis.py --list wgen_result.json -nshow
wget -O scenario.json http://api-dev01.listenfield.com:8083/wgen/kgen/naro1km/100/42.839847/143.209880/2016-01-01/2016-12-31/33:34:33
python wvis.py --list scenario.json -nshow
