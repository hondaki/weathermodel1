# README #

Experimental python or R code for weather modeling.

### What is this repository for? ###

* Overview
    * Obtain mean and stdev of the normal distribution that gives given bn, nn, an and save them as a figure
    * Adjust and Visualize gamma distribution based on bn,nn,an
         * python gamma.py --help
         * python gamma.py 1.2 50 0.2 0.3 0.5 -show
    * Visualize weather scenarios ( currently WTD files on local or http ) *** Now python3.5
         * python wvis.py --help
         * python wvis.py --list CaseStudy1_ListWTD_DisAg.xml
         * python wvis.py *.WTD
    * Generate Boxplot viewgraphs for dates/float and horizonal/vertical  *** for python3.5
         * python boxplot_viewgraph.py
    * CGI version of Generate Boxplot viewgraphs for dates/float and horizonal/vertical  *** for python3.5
         * source is in ./boxplot_cgi/
         * how to run -> exec.sh
         * prepare POST data and POST to api.listenfield.com/cgi-bin/boxplot_cgi.py
         * sample POST data sample: boxplot-h.json, boxplot-v.json ( h:horizontal, v:vertical)
         * Mandatory data, categories, direction["v"|"h"], vtyp ["date"|"float"], vmin,vmax for data rage
         * Currently result image is fixed in http://api.listenfield.com/boxplot/boxplot.png
         * The API returns the result, including quantile, outliers

* Version
    * 0.1

### How do I get set up? ###

* Summary of set up
    * python 2.7 with scipy matplotlib from https://www.scipy.org/
    * R
* Configuration
    * python may not work with virtualenv
* Dependencies
* Database configuration
* How to run tests
    * sh exec.sh
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact